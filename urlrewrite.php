<?php
$arUrlRewrite=array (
  5 => 
  array (
    'CONDITION' => '#^/docs/pub/(?<hash>[0-9a-f]{32})/(?<action>[0-9a-zA-Z]+)/\\?#',
    'RULE' => 'hash=$1&action=$2&',
    'ID' => 'bitrix:disk.external.link',
    'PATH' => '/docs/pub/index.php',
    'SORT' => 100,
  ),
  138 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year/edit/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#',
    'RULE' => '',
    'ID' => 'LEAVE_GROUP_ADD',
    'PATH' => '/company/leave/leaveyear/edit.php',
    'SORT' => 100,
  ),
  2087 => 
  array (
    'CONDITION' => '#^/overtime-plan/setting/permission/workgroup-project/#',
    'RULE' => '',
    'ID' => 'ot.setting.workgroup',
    'PATH' => '/company/overtime/setting/workgroup/index.php',
    'SORT' => 100,
  ),
  209 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/member/([0-9a-z_]+?)/import#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.member.import.data',
    'PATH' => '/company/out_office/timekeeper/member_import.php',
    'SORT' => 100,
  ),
  6 => 
  array (
    'CONDITION' => '#^/disk/(?<action>[0-9a-zA-Z]+)/(?<fileId>[0-9]+)/\\?#',
    'RULE' => 'action=$1&fileId=$2&',
    'ID' => 'bitrix:disk.services',
    'PATH' => '/bitrix/services/disk/index.php',
    'SORT' => 100,
  ),
  2196 => 
  array (
    'CONDITION' => '#^/out-office/setting/permission/workgroup-project/#',
    'RULE' => '',
    'ID' => 'out_office.setting.workgroup',
    'PATH' => '/company/out_office/setting/workgroup/index.php',
    'SORT' => 100,
  ),
  2189 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/request-to-approver/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.approver',
    'PATH' => '/company/overtime/detail_report/approver.php',
    'SORT' => 100,
  ),
  2190 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/member-in-workgroup/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.workgroup',
    'PATH' => '/company/overtime/detail_report/in_workgroup.php',
    'SORT' => 100,
  ),
  216 => 
  array (
    'CONDITION' => '#^/out-office/setting/permission/workgroup-project#',
    'RULE' => '',
    'ID' => 'out_office.setting.workgroup',
    'PATH' => '/company/out_office/setting/workgroup/index.php',
    'SORT' => 100,
  ),
  2191 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/member-in-project/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.project',
    'PATH' => '/company/overtime/detail_report/in_project.php',
    'SORT' => 100,
  ),
  2200 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/member-in-workgroup#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.workgroup',
    'PATH' => '/company/out_office/detail_report/in_workgroup.php',
    'SORT' => 100,
  ),
  2219 => 
  array (
    'CONDITION' => '#^/qua-trinh-cong-tac/([0-9]+)/([0-9]+)/detail/#',
    'RULE' => 'employeeId = $1 & id = $2',
    'ID' => 'QUA_TRINH_CONG_TAC_CHI_TIET',
    'PATH' => '/company/hr/PositionLevel/detail.php',
    'SORT' => 100,
  ),
  2199 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/request-to-approver#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.approver',
    'PATH' => '/company/out_office/detail_report/approver.php',
    'SORT' => 100,
  ),
  2211 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/history/([0-9a-z_]+?)#',
    'RULE' => 'keep_id=$1',
    'ID' => 'out_office.timekeeper.history',
    'PATH' => '/company/out_office/timekeeper/history.php',
    'SORT' => 100,
  ),
  2209 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/import/([0-9a-z_]+?)#',
    'RULE' => 'keep_id=$1',
    'ID' => 'out_office.timekeeper.import',
    'PATH' => '/company/out_office/timekeeper/import.php',
    'SORT' => 100,
  ),
  2212 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/detail/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.detail',
    'PATH' => '/company/out_office/timekeeper/detail.php',
    'SORT' => 100,
  ),
  2206 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/member/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.member',
    'PATH' => '/company/out_office/timekeeper/member.php',
    'SORT' => 100,
  ),
  2096 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/administrator/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.employee',
    'PATH' => '/company/overtime/detail_report/administrator.php',
    'SORT' => 100,
  ),
  2201 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/member-in-project#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.project',
    'PATH' => '/company/out_office/detail_report/in_project.php',
    'SORT' => 100,
  ),
  2220 => 
  array (
    'CONDITION' => '#^/qua-trinh-cong-tac/([0-9]+)/([0-9]+)/edit/#',
    'RULE' => '',
    'ID' => 'SUA_THEM_QUA_TRINH_CONG_TAC',
    'PATH' => '/company/hr/PositionLevel/edit.php',
    'SORT' => 100,
  ),
  17 => 
  array (
    'CONDITION' => '#^\\/?\\/mobile/web_mobile_component\\/(.*)\\/.*#',
    'RULE' => 'componentName=$1',
    'ID' => '',
    'PATH' => '/bitrix/services/mobile/webcomponent.php',
    'SORT' => 100,
  ),
  2183 => 
  array (
    'CONDITION' => '#^/overtime-plan/setting/permission/general/#',
    'RULE' => '',
    'ID' => 'ot.setting.general',
    'PATH' => '/company/overtime/setting/general/index.php',
    'SORT' => 100,
  ),
  2165 => 
  array (
    'CONDITION' => '#^/leaveplan/detail-report/report-to-approve#',
    'RULE' => '',
    'ID' => 'detail_report.to_approve',
    'PATH' => '/company/leave/leavereport/report_to_approve.php',
    'SORT' => 100,
  ),
  2186 => 
  array (
    'CONDITION' => '#^/overtime-plan/overtime-edit/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'overtime.request.edit',
    'PATH' => '/company/overtime/my_overtime/edit.php',
    'SORT' => 100,
  ),
  2192 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/all-employee/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.employee',
    'PATH' => '/company/overtime/detail_report/all_employee.php',
    'SORT' => 100,
  ),
  2205 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/edit/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.edit',
    'PATH' => '/company/out_office/timekeeper/edit.php',
    'SORT' => 100,
  ),
  2187 => 
  array (
    'CONDITION' => '#^/overtime-plan/overtime-plan-for-approve/#',
    'RULE' => '',
    'ID' => 'ot.for.approve',
    'PATH' => '/company/overtime/ot_approve/index.php',
    'SORT' => 100,
  ),
  2166 => 
  array (
    'CONDITION' => '#^/leaveplan/detail-report/report-inform-hr#',
    'RULE' => '',
    'ID' => 'detail_report.inform_hr',
    'PATH' => '/company/leave/leavereport/report_inform_hr.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/pub/pay/([\\w\\W]+)/([0-9a-zA-Z]+)/([^/]*)#',
    'RULE' => 'account_number=$1&hash=$2',
    'ID' => NULL,
    'PATH' => '/pub/payment.php',
    'SORT' => 100,
  ),
  2164 => 
  array (
    'CONDITION' => '#^/leaveplan/detail-report/report-personal#',
    'RULE' => '',
    'ID' => 'detail_report.personal',
    'PATH' => '/company/leave/leavereport/report_personal.php',
    'SORT' => 100,
  ),
  2202 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/all-employee/#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.employee',
    'PATH' => '/company/out_office/detail_report/all_employee.php',
    'SORT' => 100,
  ),
  41 => 
  array (
    'CONDITION' => '#^/pub/form/([0-9a-z_]+?)/([0-9a-z]+?)/.*#',
    'RULE' => 'form_code=$1&sec=$2',
    'ID' => 'bitrix:crm.webform.fill',
    'PATH' => '/pub/form.php',
    'SORT' => 100,
  ),
  16 => 
  array (
    'CONDITION' => '#^\\/?\\/mobile/mobile_component\\/(.*)\\/.*#',
    'RULE' => 'componentName=$1',
    'ID' => '',
    'PATH' => '/bitrix/services/mobile/jscomponent.php',
    'SORT' => 100,
  ),
  2195 => 
  array (
    'CONDITION' => '#^/out-office/setting/permission/general#',
    'RULE' => '',
    'ID' => 'out_office.setting.general',
    'PATH' => '/company/out_office/setting/general/index.php',
    'SORT' => 100,
  ),
  215 => 
  array (
    'CONDITION' => '#^/out-office/request-edit/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'leave.request.edit',
    'PATH' => '/company/out_office/request/edit.php',
    'SORT' => 100,
  ),
  2188 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/personal/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.personal',
    'PATH' => '/company/overtime/detail_report/personal.php',
    'SORT' => 100,
  ),
  2150 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/all-employee#',
    'RULE' => '',
    'ID' => 'out-off9.detail.report.employee',
    'PATH' => '/company/out_office/detail_report/all_employee.php',
    'SORT' => 100,
  ),
  2208 => 
  array (
    'CONDITION' => '#^/out-offices/member/edit/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.member.edit',
    'PATH' => '/company/out_office/timekeeper/member_edit.php',
    'SORT' => 100,
  ),
  2173 => 
  array (
    'CONDITION' => '#^/leaveplan/detail-report/report-cc-hr#',
    'RULE' => '',
    'ID' => 'detail_report.cc_hr',
    'PATH' => '/company/leave/leavereport/report_cc_hr.php',
    'SORT' => 100,
  ),
  11 => 
  array (
    'CONDITION' => '#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#',
    'RULE' => 'alias=$1',
    'ID' => NULL,
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  2207 => 
  array (
    'CONDITION' => '#^/out-offices/member/([0-9a-z_]+?)/add#',
    'RULE' => 'id=$1',
    'ID' => 'out_office.timekeeper.member.add',
    'PATH' => '/company/out_office/timekeeper/member_add.php',
    'SORT' => 100,
  ),
  2218 => 
  array (
    'CONDITION' => '#^/nhan-su/([0-9]+)/qua-trinh-cong-tac/#',
    'RULE' => '',
    'ID' => 'QUA_TRINH_CONG_TAC',
    'PATH' => '/company/hr/PositionLevel/positionLevel.php',
    'SORT' => 100,
  ),
  18 => 
  array (
    'CONDITION' => '#^/mobile/disk/(?<hash>[0-9]+)/download#',
    'RULE' => 'download=1&objectId=$1',
    'ID' => 'bitrix:mobile.disk.file.detail',
    'PATH' => '/mobile/disk/index.php',
    'SORT' => 100,
  ),
  2198 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/personal/#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.personal',
    'PATH' => '/company/out-office/detail_report/personal.php',
    'SORT' => 100,
  ),
  2169 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-edit/([0-9a-z_]+?)#',
    'RULE' => 'id=$1',
    'ID' => 'leave.request.edit',
    'PATH' => '/company/leave/leaverequest/edit.php',
    'SORT' => 100,
  ),
  2146 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/personal#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.personal',
    'PATH' => '/company/out_office/detail_report/index.php',
    'SORT' => 100,
  ),
  34 => 
  array (
    'CONDITION' => '#^/tasks/getfile/(\\d+)/(\\d+)/([^/]+)#',
    'RULE' => 'taskid=$1&fileid=$2&filename=$3',
    'ID' => 'bitrix:tasks_tools_getfile',
    'PATH' => '/tasks/getfile.php',
    'SORT' => 100,
  ),
  2145 => 
  array (
    'CONDITION' => '#^/out-office/out-office-for-approve#',
    'RULE' => '',
    'ID' => 'out_office.for.approve',
    'PATH' => '/company/out_office/approve/index.php',
    'SORT' => 100,
  ),
  224 => 
  array (
    'CONDITION' => '#detail-report/report-all-employee/#',
    'RULE' => '',
    'ID' => 'thangsss',
    'PATH' => '/company/leave/leavereport/report_all_employee.php',
    'SORT' => 100,
  ),
  73 => 
  array (
    'CONDITION' => '#^/crm/configs/document_numerators/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.document_numerators.list',
    'PATH' => '/crm/configs/document_numerators/index.php',
    'SORT' => 100,
  ),
  2214 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/employee#',
    'RULE' => '',
    'ID' => 'out_office.timekeeper.report.employee',
    'PATH' => '/company/out_office/timekeeper/employee.php',
    'SORT' => 100,
  ),
  2099 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/hr/#',
    'RULE' => '',
    'ID' => 'ot.detail.report.hr',
    'PATH' => '/company/overtime/detail_report/hr.php',
    'SORT' => 100,
  ),
  2210 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/download#',
    'RULE' => '',
    'ID' => 'out_office.timekeeper.download',
    'PATH' => '/company/out_office/timekeeper/download.php',
    'SORT' => 100,
  ),
  2213 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/personal#',
    'RULE' => '',
    'ID' => 'out_office.timekeeper.report.personal',
    'PATH' => '/company/out_office/timekeeper/personal.php',
    'SORT' => 100,
  ),
  8 => 
  array (
    'CONDITION' => '#^/stssync/contacts_extranet_emp/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/contacts_extranet_emp/index.php',
    'SORT' => 100,
  ),
  2172 => 
  array (
    'CONDITION' => '#^/leaveplan/summary-all-employee#',
    'RULE' => '',
    'ID' => 'LEAVE_SUMMARY_REPORT_ALL_EMPLOYEE',
    'PATH' => '/company/leave/summary_report/allEmployee.php',
    'SORT' => 100,
  ),
  2159 => 
  array (
    'CONDITION' => '#^/timesheet/permission-workgroup#',
    'RULE' => '',
    'ID' => 'timesheet:permisstion.workgroup',
    'PATH' => '/company/timesheet/permisstion.workgroup/index.php',
    'SORT' => 100,
  ),
  2184 => 
  array (
    'CONDITION' => '#^/overtime-plan/overtime/general#',
    'RULE' => '',
    'ID' => 'ot.my.overtime',
    'PATH' => '/company/overtime/my_overtime/index.php',
    'SORT' => 100,
  ),
  2174 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-type/download#',
    'RULE' => '',
    'ID' => 'leave.download',
    'PATH' => '/company/leave/leavetype/download.php',
    'SORT' => 100,
  ),
  555 => 
  array (
    'CONDITION' => '#detail-report/report-to-approve#',
    'RULE' => '',
    'ID' => 'thang',
    'PATH' => '/company/leave/leavereport/report_to_approve.php',
    'SORT' => 100,
  ),
  2144 => 
  array (
    'CONDITION' => '#^/out-office/out-office-request#',
    'RULE' => '',
    'ID' => 'out_office.request.add',
    'PATH' => '/company/out_office/request/add.php',
    'SORT' => 100,
  ),
  45 => 
  array (
    'CONDITION' => '#^/settings/configs/userconsent/#',
    'RULE' => '',
    'ID' => 'bitrix:intranet.userconsent',
    'PATH' => '/configs/userconsent.php',
    'SORT' => 100,
  ),
  2090 => 
  array (
    'CONDITION' => '#^/overtime-plan/detail-report/#',
    'RULE' => '',
    'ID' => 'ot.detail.report',
    'PATH' => '/company/overtime/detail_report/index.php',
    'SORT' => 100,
  ),
  2163 => 
  array (
    'CONDITION' => '#^/leaveplan/permission/general#',
    'RULE' => '',
    'ID' => 'leave.setting.general',
    'PATH' => '/company/leave/setting/general/index.php',
    'SORT' => 100,
  ),
  222 => 
  array (
    'CONDITION' => '#detail-report/report-inform-hr#',
    'RULE' => '',
    'ID' => 'thang',
    'PATH' => '/company/leave/leavereport/report_inform_hr.php',
    'SORT' => 100,
  ),
  111 => 
  array (
    'CONDITION' => '#detail-report/report-personal#',
    'RULE' => '',
    'ID' => 'thang',
    'PATH' => '/company/leave/leavereport/report_personal.php',
    'SORT' => 100,
  ),
  85 => 
  array (
    'CONDITION' => '#^/extranet/contacts/personal/#',
    'RULE' => NULL,
    'ID' => 'bitrix:socialnetwork_user',
    'PATH' => '/extranet/contacts/personal.php',
    'SORT' => 100,
  ),
  2181 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-type/import#',
    'RULE' => '',
    'ID' => 'LEAVE_TYPE_IMPORT',
    'PATH' => '/company/leave/leavetype/import.php',
    'SORT' => 100,
  ),
  2171 => 
  array (
    'CONDITION' => '#^/leaveplan/summary-inform-hr#',
    'RULE' => '',
    'ID' => 'LEAVE_SUMMARY_REPORT_HR',
    'PATH' => '/company/leave/summary_report/informHr.php',
    'SORT' => 100,
  ),
  2176 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year/import#',
    'RULE' => '',
    'ID' => 'LEAVE_YEAR_IMPORT',
    'PATH' => '/company/leave/leaveyear/import.php',
    'SORT' => 100,
  ),
  255 => 
  array (
    'CONDITION' => '#^/out-office/detail-report/hr#',
    'RULE' => '',
    'ID' => 'out_office.detail.report.workgroup',
    'PATH' => '/company/out_office/detail_report/hr.php',
    'SORT' => 100,
  ),
  197 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year/export#',
    'RULE' => '',
    'ID' => 'timesheet:information',
    'PATH' => '/company/leave/leaveyear/export_excel.php',
    'SORT' => 100,
  ),
  84 => 
  array (
    'CONDITION' => '#^/extranet/workgroups/create/#',
    'RULE' => NULL,
    'ID' => 'bitrix:extranet.group_create',
    'PATH' => '/extranet/workgroups/create/index.php',
    'SORT' => 100,
  ),
  2067 => 
  array (
    'CONDITION' => '#^/leaveplan/approve/transfer#',
    'RULE' => '',
    'ID' => 'leave:approve_transfer',
    'PATH' => '/company/leave/leaveapprove/transfer.php',
    'SORT' => 100,
  ),
  2185 => 
  array (
    'CONDITION' => '#^/overtime-plan/overtime/add#',
    'RULE' => '',
    'ID' => 'ot.my.overtime.add',
    'PATH' => '/company/overtime/my_overtime/add.php',
    'SORT' => 100,
  ),
  10 => 
  array (
    'CONDITION' => '#^/stssync/calendar_extranet/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/calendar_extranet/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/stssync/contacts_extranet/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/contacts_extranet/index.php',
    'SORT' => 100,
  ),
  71 => 
  array (
    'CONDITION' => '#^/crm/configs/deal_category/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.deal_category',
    'PATH' => '/crm/configs/deal_category/index.php',
    'SORT' => 100,
  ),
  2204 => 
  array (
    'CONDITION' => '#^/out-offices/timekeeper/add#',
    'RULE' => '',
    'ID' => 'out_office.timekeeper.add',
    'PATH' => '/company/out_office/timekeeper/add.php',
    'SORT' => 100,
  ),
  2170 => 
  array (
    'CONDITION' => '#^/leaveplan/summary/approve#',
    'RULE' => '',
    'ID' => 'LEAVE_SUMMARY_REPORT_APPROVE',
    'PATH' => '/company/leave/summary_report/approve.php',
    'SORT' => 100,
  ),
  223 => 
  array (
    'CONDITION' => '#detail-report/report-cc-hr/#',
    'RULE' => '',
    'ID' => 'thangss',
    'PATH' => '/company/leave/leavereport/report_cc_hr.php',
    'SORT' => 100,
  ),
  67 => 
  array (
    'CONDITION' => '#^/crm/configs/productprops/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.productprops',
    'PATH' => '/crm/configs/productprops/index.php',
    'SORT' => 100,
  ),
  63 => 
  array (
    'CONDITION' => '#^/crm/configs/mailtemplate/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.mail_template',
    'PATH' => '/crm/configs/mailtemplate/index.php',
    'SORT' => 100,
  ),
  114 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-group/add#',
    'RULE' => '',
    'ID' => 'LEAVE_GROUP_ADD',
    'PATH' => '/company/leave/leavegroup/add.php',
    'SORT' => 100,
  ),
  2180 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-type/add#',
    'RULE' => '',
    'ID' => 'LEAVE_TYPE_ADD',
    'PATH' => '/company/leave/leavetype/add.php',
    'SORT' => 100,
  ),
  2156 => 
  array (
    'CONDITION' => '#^/timesheet/company-report#',
    'RULE' => '',
    'ID' => 'timesheet:company_report',
    'PATH' => '/company/timesheet/company_report/index.php',
    'SORT' => 100,
  ),
  24 => 
  array (
    'CONDITION' => '#^/bitrix/services/ymarket/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/services/ymarket/index.php',
    'SORT' => 100,
  ),
  2155 => 
  array (
    'CONDITION' => '#^/timesheet/summary-report#',
    'RULE' => '',
    'ID' => 'timesheet:summary_report',
    'PATH' => '/company/timesheet/summary_report/index.php',
    'SORT' => 100,
  ),
  200 => 
  array (
    'CONDITION' => '#^/leaveplan/summary-report#',
    'RULE' => '',
    'ID' => 'LEAVE_SUMMARY_REPORT',
    'PATH' => '/company/leave/summary_report/member.php',
    'SORT' => 100,
  ),
  137 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year/add#',
    'RULE' => '',
    'ID' => 'LEAVE_GROUP_ADD',
    'PATH' => '/company/leave/leaveyear/add.php',
    'SORT' => 100,
  ),
  174 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-request#',
    'RULE' => '',
    'ID' => 'LEAVE_REQUEST',
    'PATH' => '/company/leave/leaverequest/index.php',
    'SORT' => 100,
  ),
  2177 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-convert#',
    'RULE' => '',
    'ID' => 'LEAVE_CONVERT',
    'PATH' => '/company/leave/leaveconvert/index.php',
    'SORT' => 100,
  ),
  217 => 
  array (
    'CONDITION' => '#^/out-office/request-list#',
    'RULE' => '',
    'ID' => 'out_office.request.list',
    'PATH' => '/company/out_office/request/index.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/stssync/tasks_extranet/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/tasks_extranet/index.php',
    'SORT' => 100,
  ),
  54 => 
  array (
    'CONDITION' => '#^/crm/configs/automation/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.automation',
    'PATH' => '/crm/configs/automation/index.php',
    'SORT' => 100,
  ),
  205 => 
  array (
    'CONDITION' => '#^/leaveplan/summary-other#',
    'RULE' => '',
    'ID' => 'LEAVE_SUMMARY_REPORT_OTHER',
    'PATH' => '/company/leave/summary_report/other.php',
    'SORT' => 100,
  ),
  2193 => 
  array (
    'CONDITION' => '#^/overtime-plan/dashbroad#',
    'RULE' => '',
    'ID' => 'ot.detail.dashbroad',
    'PATH' => '/company/overtime/dashbroad/index.php',
    'SORT' => 100,
  ),
  86 => 
  array (
    'CONDITION' => '#^/extranet/mobile/webdav#',
    'RULE' => NULL,
    'ID' => 'bitrix:mobile.webdav.file.list',
    'PATH' => '/extranet/mobile/webdav/index.php',
    'SORT' => 100,
  ),
  2215 => 
  array (
    'CONDITION' => '#^/admin/general-setting/#',
    'RULE' => '',
    'ID' => 'GENERAL_SETTING',
    'PATH' => '/company/hr/generalSetting/general_setting.php',
    'SORT' => 100,
  ),
  70 => 
  array (
    'CONDITION' => '#^/crm/configs/mycompany/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.company',
    'PATH' => '/crm/configs/mycompany/index.php',
    'SORT' => 100,
  ),
  60 => 
  array (
    'CONDITION' => '#^/crm/configs/locations/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.locations',
    'PATH' => '/crm/configs/locations/index.php',
    'SORT' => 100,
  ),
  27 => 
  array (
    'CONDITION' => '#^/company/personal/mail/#',
    'RULE' => '',
    'ID' => 'bitrix:intranet.mail.config',
    'PATH' => '/company/personal/mail/index.php',
    'SORT' => 100,
  ),
  82 => 
  array (
    'CONDITION' => '#^/marketing/config/role/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/config/role.php',
    'SORT' => 100,
  ),
  43 => 
  array (
    'CONDITION' => '#^/crm/configs/exclusion/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/crm/configs/exclusion/index.php',
    'SORT' => 100,
  ),
  218 => 
  array (
    'CONDITION' => '#^/out-office/request-add#',
    'RULE' => '',
    'ID' => 'out_office.request.add',
    'PATH' => '/company/out_office/request/add.php',
    'SORT' => 100,
  ),
  2203 => 
  array (
    'CONDITION' => '#^/out-office/timekeeper#',
    'RULE' => '',
    'ID' => 'out_office.timekeeper',
    'PATH' => '/company/out_office/timekeeper/index.php',
    'SORT' => 100,
  ),
  2153 => 
  array (
    'CONDITION' => '#^/timesheet/information#',
    'RULE' => '',
    'ID' => 'timesheet:information',
    'PATH' => '/company/timesheet/information/index.php',
    'SORT' => 100,
  ),
  58 => 
  array (
    'CONDITION' => '#^/crm/configs/currency/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.currency',
    'PATH' => '/crm/configs/currency/index.php',
    'SORT' => 100,
  ),
  2158 => 
  array (
    'CONDITION' => '#^/timesheet/permisstion#',
    'RULE' => '',
    'ID' => 'timesheet:permisstion',
    'PATH' => '/company/timesheet/permisstion/index.php',
    'SORT' => 100,
  ),
  2178 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-group#',
    'RULE' => '',
    'ID' => 'LEAVE_GROUP_LIST',
    'PATH' => '/company/leave/leavegroup/index.php',
    'SORT' => 100,
  ),
  2066 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year/#',
    'RULE' => '',
    'ID' => 'LEAVE_YEAR',
    'PATH' => '/company/leave/leaveyear/index.php',
    'SORT' => 100,
  ),
  3 => 
  array (
    'CONDITION' => '#^/stssync/contacts_crm/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/contacts_crm/index.php',
    'SORT' => 100,
  ),
  136 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-year#',
    'RULE' => '',
    'ID' => 'LEAVE_YEAR_LIST',
    'PATH' => '/company/leave/leaveyear/index.php',
    'SORT' => 100,
  ),
  2167 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-grid#',
    'RULE' => '',
    'ID' => 'leave.request.grid',
    'PATH' => '/company/leave/leaverequest/grid.php',
    'SORT' => 100,
  ),
  83 => 
  array (
    'CONDITION' => '#^/extranet/workgroups/#',
    'RULE' => NULL,
    'ID' => 'bitrix:socialnetwork_group',
    'PATH' => '/extranet/workgroups/index.php',
    'SORT' => 100,
  ),
  79 => 
  array (
    'CONDITION' => '#^/marketing/blacklist/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/blacklist.php',
    'SORT' => 100,
  ),
  162 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-move#',
    'RULE' => '',
    'ID' => 'leave:leavemove',
    'PATH' => '/company/leave/leavemove/index.php',
    'SORT' => 100,
  ),
  65 => 
  array (
    'CONDITION' => '#^/crm/configs/measure/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.measure',
    'PATH' => '/crm/configs/measure/index.php',
    'SORT' => 100,
  ),
  2179 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-type#',
    'RULE' => '',
    'ID' => 'LEAVE_TYPE_LIST',
    'PATH' => '/company/leave/leavetype/index.php',
    'SORT' => 100,
  ),
  96 => 
  array (
    'CONDITION' => '#^/timesheet/projectlog#',
    'RULE' => '',
    'ID' => 'timesheet:projectlog',
    'PATH' => '/company/timesheet/project_log/index.php',
    'SORT' => 100,
  ),
  2194 => 
  array (
    'CONDITION' => '#^/out-office/dashbroad#',
    'RULE' => '',
    'ID' => 'out_office.dashbroad',
    'PATH' => '/company/out_office/dashbroad/index.php',
    'SORT' => 100,
  ),
  2157 => 
  array (
    'CONDITION' => '#^/timesheet/qa-report#',
    'RULE' => '',
    'ID' => 'timesheet:qa_report',
    'PATH' => '/company/timesheet/qa_report/index.php',
    'SORT' => 100,
  ),
  68 => 
  array (
    'CONDITION' => '#^/crm/configs/preset/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.preset',
    'PATH' => '/crm/configs/preset/index.php',
    'SORT' => 100,
  ),
  66 => 
  array (
    'CONDITION' => '#^/crm/configs/volume/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.volume',
    'PATH' => '/crm/configs/volume/index.php',
    'SORT' => 100,
  ),
  2162 => 
  array (
    'CONDITION' => '#^/timesheet/dashbroad#',
    'RULE' => '',
    'ID' => 'timesheet:dashbroad',
    'PATH' => '/company/timesheet/dashbroad/index.php',
    'SORT' => 100,
  ),
  2221 => 
  array (
    'CONDITION' => '#^/([0-9]+)/chuc-danh/#',
    'RULE' => '',
    'ID' => 'DANH_SACH_CHUC_DANH',
    'PATH' => '/company/hr/TitleManagement/index.php',
    'SORT' => 100,
  ),
  53 => 
  array (
    'CONDITION' => '#^/crm/configs/fields/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.fields',
    'PATH' => '/crm/configs/fields/index.php',
    'SORT' => 100,
  ),
  62 => 
  array (
    'CONDITION' => '#^/crm/reports/report/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.report',
    'PATH' => '/crm/reports/report/index.php',
    'SORT' => 100,
  ),
  208 => 
  array (
    'CONDITION' => '#^/setting/setup-admin#',
    'RULE' => '',
    'ID' => 'setup.admin',
    'PATH' => '/company/setting/index.php',
    'SORT' => 100,
  ),
  78 => 
  array (
    'CONDITION' => '#^/marketing/template/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/template.php',
    'SORT' => 100,
  ),
  64 => 
  array (
    'CONDITION' => '#^/crm/configs/exch1c/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.exch1c',
    'PATH' => '/crm/configs/exch1c/index.php',
    'SORT' => 100,
  ),
  2168 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-add#',
    'RULE' => '',
    'ID' => 'leave.request.add',
    'PATH' => '/company/leave/leaverequest/add.php',
    'SORT' => 100,
  ),
  2182 => 
  array (
    'CONDITION' => '#^/leaveplan/dashbroad#',
    'RULE' => '',
    'ID' => 'LEAVE_DASHBROAD',
    'PATH' => '/company/leave/dashbroad/index.php',
    'SORT' => 100,
  ),
  12 => 
  array (
    'CONDITION' => '#^/online/(/?)([^/]*)#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  21 => 
  array (
    'CONDITION' => '#^/marketplace/local/#',
    'RULE' => '',
    'ID' => 'bitrix:rest.marketplace.localapp',
    'PATH' => '/marketplace/local/index.php',
    'SORT' => 100,
  ),
  80 => 
  array (
    'CONDITION' => '#^/marketing/contact/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/contact.php',
    'SORT' => 100,
  ),
  2068 => 
  array (
    'CONDITION' => '#^/leaveplan/approve/#',
    'RULE' => '',
    'ID' => 'leave:approve',
    'PATH' => '/company/leave/leaveapprove/index.php',
    'SORT' => 100,
  ),
  56 => 
  array (
    'CONDITION' => '#^/crm/configs/perms/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.perms',
    'PATH' => '/crm/configs/perms/index.php',
    'SORT' => 100,
  ),
  77 => 
  array (
    'CONDITION' => '#^/marketing/segment/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/segment.php',
    'SORT' => 100,
  ),
  161 => 
  array (
    'CONDITION' => '#^/leaveplan/leave-hr#',
    'RULE' => '',
    'ID' => 'LEAVE_HR',
    'PATH' => '/company/leave/hr_form/index.php',
    'SORT' => 100,
  ),
  2154 => 
  array (
    'CONDITION' => '#^/timesheet/settings#',
    'RULE' => '',
    'ID' => 'timesheet:settings',
    'PATH' => '/company/timesheet/settings/index.php',
    'SORT' => 100,
  ),
  40 => 
  array (
    'CONDITION' => '#^/bizproc/processes/#',
    'RULE' => '',
    'ID' => 'bitrix:lists',
    'PATH' => '/bizproc/processes/index.php',
    'SORT' => 100,
  ),
  2152 => 
  array (
    'CONDITION' => '#^/timesheet/approved#',
    'RULE' => '',
    'ID' => 'timesheet:approved',
    'PATH' => '/company/timesheet/approved/index.php',
    'SORT' => 100,
  ),
  91 => 
  array (
    'CONDITION' => '#^/shop/buyer_group/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.order.buyer_group',
    'PATH' => '/shop/buyer_group/index.php',
    'SORT' => 100,
  ),
  2161 => 
  array (
    'CONDITION' => '#^/timesheet/convert#',
    'RULE' => '',
    'ID' => 'timesheet:convert',
    'PATH' => '/company/timesheet/convert/index.php',
    'SORT' => 100,
  ),
  23 => 
  array (
    'CONDITION' => '#^/marketplace/hook/#',
    'RULE' => '',
    'ID' => 'bitrix:rest.hook',
    'PATH' => '/marketplace/hook/index.php',
    'SORT' => 100,
  ),
  94 => 
  array (
    'CONDITION' => '#^/timesheet/worklog#',
    'RULE' => '',
    'ID' => 'timesheet:worklog',
    'PATH' => '/company/timesheet/work_log/index.php',
    'SORT' => 100,
  ),
  28 => 
  array (
    'CONDITION' => '#^/company/personal/#',
    'RULE' => '',
    'ID' => 'bitrix:socialnetwork_user',
    'PATH' => '/company/personal.php',
    'SORT' => 100,
  ),
  75 => 
  array (
    'CONDITION' => '#^/marketing/letter/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/letter.php',
    'SORT' => 100,
  ),
  2160 => 
  array (
    'CONDITION' => '#^/timesheet/holiday#',
    'RULE' => '',
    'ID' => 'timesheet:holiday',
    'PATH' => '/company/timesheet/holiday/index.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/stssync/calendar/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/calendar/index.php',
    'SORT' => 100,
  ),
  13 => 
  array (
    'CONDITION' => '#^/stssync/contacts/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/contacts/index.php',
    'SORT' => 100,
  ),
  59 => 
  array (
    'CONDITION' => '#^/crm/configs/tax/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.tax',
    'PATH' => '/crm/configs/tax/index.php',
    'SORT' => 100,
  ),
  2080 => 
  array (
    'CONDITION' => '#^/timesheet/report#',
    'RULE' => '',
    'ID' => 'timesheet:report',
    'PATH' => '/company/timesheet/report/index.php',
    'SORT' => 100,
  ),
  2175 => 
  array (
    'CONDITION' => '#^/leaveplan/makeup#',
    'RULE' => '',
    'ID' => 'leave.makeup',
    'PATH' => '/company/leave/makeup/index.php',
    'SORT' => 100,
  ),
  74 => 
  array (
    'CONDITION' => '#^/timeman/meeting/#',
    'RULE' => '',
    'ID' => 'bitrix:meetings',
    'PATH' => '/timeman/meeting/index.php',
    'SORT' => 100,
  ),
  26 => 
  array (
    'CONDITION' => '#^/company/gallery/#',
    'RULE' => '',
    'ID' => 'bitrix:photogallery_user',
    'PATH' => '/company/gallery/index.php',
    'SORT' => 100,
  ),
  2151 => 
  array (
    'CONDITION' => '#^/timesheet/config#',
    'RULE' => '',
    'ID' => 'timesheet:config',
    'PATH' => '/company/timesheet/config/index.php',
    'SORT' => 100,
  ),
  22 => 
  array (
    'CONDITION' => '#^/marketplace/app/#',
    'RULE' => '',
    'ID' => 'bitrix:app.layout',
    'PATH' => '/marketplace/app/index.php',
    'SORT' => 100,
  ),
  55 => 
  array (
    'CONDITION' => '#^/crm/configs/bp/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.bp',
    'PATH' => '/crm/configs/bp/index.php',
    'SORT' => 100,
  ),
  31 => 
  array (
    'CONDITION' => '#^/services/lists/#',
    'RULE' => '',
    'ID' => 'bitrix:lists',
    'PATH' => '/services/lists/index.php',
    'SORT' => 100,
  ),
  61 => 
  array (
    'CONDITION' => '#^/crm/configs/ps/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.config.ps',
    'PATH' => '/crm/configs/ps/index.php',
    'SORT' => 100,
  ),
  90 => 
  array (
    'CONDITION' => '#^/shop/orderform/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.order.matcher',
    'PATH' => '/shop/orderform/index.php',
    'SORT' => 100,
  ),
  46 => 
  array (
    'CONDITION' => '#^/services/wiki/#',
    'RULE' => '',
    'ID' => 'bitrix:wiki',
    'PATH' => '/services/wiki.php',
    'SORT' => 100,
  ),
  29 => 
  array (
    'CONDITION' => '#^/about/gallery/#',
    'RULE' => '',
    'ID' => 'bitrix:photogallery',
    'PATH' => '/about/gallery/index.php',
    'SORT' => 100,
  ),
  76 => 
  array (
    'CONDITION' => '#^/marketing/ads/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/ads.php',
    'SORT' => 100,
  ),
  33 => 
  array (
    'CONDITION' => '#^/services/idea/#',
    'RULE' => '',
    'ID' => 'bitrix:idea',
    'PATH' => '/services/idea/index.php',
    'SORT' => 100,
  ),
  25 => 
  array (
    'CONDITION' => '#^/stssync/tasks/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/tasks/index.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/crm/invoicing/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/crm/invoicing/index.php',
    'SORT' => 100,
  ),
  87 => 
  array (
    'CONDITION' => '#^/shop/settings/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.admin.page.controller',
    'PATH' => '/shop/settings/index.php',
    'SORT' => 100,
  ),
  15 => 
  array (
    'CONDITION' => '#^/mobile/webdav#',
    'RULE' => '',
    'ID' => 'bitrix:mobile.webdav.file.list',
    'PATH' => '/mobile/webdav/index.php',
    'SORT' => 100,
  ),
  72 => 
  array (
    'CONDITION' => '#^/crm/activity/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.activity',
    'PATH' => '/crm/activity/index.php',
    'SORT' => 100,
  ),
  32 => 
  array (
    'CONDITION' => '#^/services/faq/#',
    'RULE' => '',
    'ID' => 'bitrix:support.faq',
    'PATH' => '/services/faq/index.php',
    'SORT' => 100,
  ),
  81 => 
  array (
    'CONDITION' => '#^/marketing/rc/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/marketing/rc.php',
    'SORT' => 100,
  ),
  48 => 
  array (
    'CONDITION' => '#^/crm/contact/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.contact',
    'PATH' => '/crm/contact/index.php',
    'SORT' => 100,
  ),
  88 => 
  array (
    'CONDITION' => '#^/shop/stores/#',
    'RULE' => '',
    'ID' => 'bitrix:landing.start',
    'PATH' => '/shop/stores/index.php',
    'SORT' => 100,
  ),
  52 => 
  array (
    'CONDITION' => '#^/crm/invoice/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.invoice',
    'PATH' => '/crm/invoice/index.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/\\.well-known#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/groupdav.php',
    'SORT' => 100,
  ),
  69 => 
  array (
    'CONDITION' => '#^/crm/webform/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.webform',
    'PATH' => '/crm/webform/index.php',
    'SORT' => 100,
  ),
  57 => 
  array (
    'CONDITION' => '#^/crm/product/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.product',
    'PATH' => '/crm/product/index.php',
    'SORT' => 100,
  ),
  20 => 
  array (
    'CONDITION' => '#^/marketplace/#',
    'RULE' => '',
    'ID' => 'bitrix:rest.marketplace',
    'PATH' => '/marketplace/index.php',
    'SORT' => 100,
  ),
  39 => 
  array (
    'CONDITION' => '#^/docs/manage/#',
    'RULE' => '',
    'ID' => 'bitrix:disk.common',
    'PATH' => '/docs/manage/index.php',
    'SORT' => 100,
  ),
  49 => 
  array (
    'CONDITION' => '#^/crm/company/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.company',
    'PATH' => '/crm/company/index.php',
    'SORT' => 100,
  ),
  89 => 
  array (
    'CONDITION' => '#^/shop/orders/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.order',
    'PATH' => '/shop/orders/index.php',
    'SORT' => 100,
  ),
  42 => 
  array (
    'CONDITION' => '#^/crm/button/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.button',
    'PATH' => '/crm/button/index.php',
    'SORT' => 100,
  ),
  103 => 
  array (
    'CONDITION' => '#^/crm/stores/#',
    'RULE' => '',
    'ID' => 'users:stores',
    'PATH' => '/crm/stores/index.php',
    'SORT' => 100,
  ),
  92 => 
  array (
    'CONDITION' => '#^/shop/buyer/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.order.buyer',
    'PATH' => '/shop/buyer/index.php',
    'SORT' => 100,
  ),
  30 => 
  array (
    'CONDITION' => '#^/workgroups/#',
    'RULE' => '',
    'ID' => 'bitrix:socialnetwork_group',
    'PATH' => '/workgroups/index.php',
    'SORT' => 100,
  ),
  38 => 
  array (
    'CONDITION' => '#^/docs/shared#',
    'RULE' => '',
    'ID' => 'bitrix:disk.common',
    'PATH' => '/docs/shared/index.php',
    'SORT' => 100,
  ),
  37 => 
  array (
    'CONDITION' => '#^/docs/sale/#',
    'RULE' => '',
    'ID' => 'bitrix:disk.common',
    'PATH' => '/docs/sale/index.php',
    'SORT' => 100,
  ),
  51 => 
  array (
    'CONDITION' => '#^/crm/quote/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.quote',
    'PATH' => '/crm/quote/index.php',
    'SORT' => 100,
  ),
  35 => 
  array (
    'CONDITION' => '#^/docs/pub/#',
    'RULE' => '',
    'ID' => 'bitrix:disk.external.link',
    'PATH' => '/docs/pub/extlinks.php',
    'SORT' => 100,
  ),
  36 => 
  array (
    'CONDITION' => '#^//docs/all#',
    'RULE' => '',
    'ID' => 'bitrix:disk.aggregator',
    'PATH' => '/docs/index.php',
    'SORT' => 100,
  ),
  93 => 
  array (
    'CONDITION' => '#^/pub/site/#',
    'RULE' => NULL,
    'ID' => 'bitrix:landing.pub',
    'PATH' => '/pub/site/index.php',
    'SORT' => 100,
  ),
  47 => 
  array (
    'CONDITION' => '#^/crm/lead/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.lead',
    'PATH' => '/crm/lead/index.php',
    'SORT' => 100,
  ),
  50 => 
  array (
    'CONDITION' => '#^/crm/deal/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.deal',
    'PATH' => '/crm/deal/index.php',
    'SORT' => 100,
  ),
  2216 => 
  array (
    'CONDITION' => '#^/nhan-su/#',
    'RULE' => '',
    'ID' => 'EMPLOYEE',
    'PATH' => '/company/hr/HR/hr.php',
    'SORT' => 100,
  ),
  14 => 
  array (
    'CONDITION' => '#^/sites/#',
    'RULE' => NULL,
    'ID' => 'bitrix:landing.start',
    'PATH' => '/sites/index.php',
    'SORT' => 100,
  ),
  19 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  44 => 
  array (
    'CONDITION' => '#^/onec/#',
    'RULE' => '',
    'ID' => 'bitrix:crm.1c.start',
    'PATH' => '/onec/index.php',
    'SORT' => 100,
  ),
  1904 => 
  array (
    'CONDITION' => '#^/cap-bac-nhan-su#',
    'RULE' => '',
    'ID' => 'RANK',
    'PATH' => '/company/hr/HR/rank.php',
    'SORT' => 100,
  )
);

