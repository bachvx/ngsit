<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;

$errors = $arResult['ERRORS'];
foreach ($errors as $error) {
    ShowError($error->getMessage());
}
$this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap-datepicker.standalone.min.css');
// $this->addExternalCss('/local/components/ngs.hr/assets/css/fontawesome-free-5.12.1/css/all.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/chosen.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/chosenImage.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/gijgo.min.css');
// $this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap-datepicker.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/jquery-ui.css');
$this->addExternalCss('/local/components/ngs.hr/employees.edit/templates/.default/css/style.css');
$title=Loc::getMessage('HR_SAVE');
?>
<div class="container">
<form class="horizontal-form"  id="frm-add-employee" method="post" enctype="multipart/form-data"> 
        <div class="material-card card" style="margin: 10px;">
            <div class="card-body wizard-content"> 
                <div class="form-group row">

            <div class="col-md-4">
                <div class="well" >
                    <label class="main-buttons-item-text-title">Mã cấp cán bộ cha</label><label class="cannot-be-empty">(*)</label>
                </div>
                <div class="well" >
                    <input id="upper_code" class="form-control" name="upper_code" type="text" maxlength="15"
                                        value="<?= $arResult['EMPLOYEE']['UPPER_CODE'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="well" >
                    <label class="main-buttons-item-text-title">Mã cấp cán bộ con</label><label class="cannot-be-empty">(*)</label>
                </div>
                <div class="well" >
                    <input id="code" class="form-control" name="code" type="text" maxlength="15"
                                        value="<?= $arResult['EMPLOYEE']['CODE'] ?>">
                </div>
            </div>
           
        <div class="col-md-2">
                <div class="well" >
                    <label class="main-buttons-item-text-title">Tình trạng sử dụng</label><label class="cannot-be-empty">(*)</label>
                </div>
                <div class="well" >
                     <select class="form-control" name="status" id="status">
                        <option  <?if($arResult['EMPLOYEE']['STATUS']=='Có'):?>selected<?endif;?> value="Có">Có</option>
                        <option  <?if($arResult['EMPLOYEE']['STATUS']=='Không'):?>selected<?endif;?> value="Không">Không</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="well" >
                    
                </div>
            <div class=" well pagetitle-container pagetitle-align-right-container" style="margin-left: 10px;margin-top: 28px;">
                 <button  type="submit" id="btn_save" class="btn btn-outline-primary float-right form-control" style="width: 150px; margin-right: 10px;" name="btnsubmit" value="save" >Save</button>
            </div>
            </div>
                </div>
            </div>
        </div>
    </form>  
</div> <!-- class="container-fluid" -->
<? 
$this->addExternalJs('/local/components/ngs.hr/assets/js/jquery-3.4.1.min.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/bootstrap.min.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/chosen.jquery.min.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/chosenImage.jquery.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/gijgo.min.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/bootstrap-datepicker.min.js');
// $this->addExternalJs('http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js');
$this->addExternalJs('/local/components/ngs.hr/assets/js/jquery-ui.min.js');
$this->addExternalJs('/local/components/ngs.hr/employees.edit/templates/.default/js/customs.js');
?>  
