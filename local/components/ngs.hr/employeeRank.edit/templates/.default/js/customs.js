var str_patt=/^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZa-zA-Z]+$/g
var str_patt_add=/^[-aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZa-zA-Z0-9]+$/g
var str_patt_num=/^[0-9]+$/g
var str_patt_email=/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
var validationFailed = false;
$(document).ready(function () {
/////////////////////////VILDATE INPUT WITH CUSTOMIZED NOTICE////////////////////////////
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                switch (e.srcElement.id) {
                    case "inp_staff_code":
                        e.target.setCustomValidity("Mã nhân viên: Không được để trống");
                        break;
                    case "inp_full_name":
                        e.target.setCustomValidity("Họ và tên: Không được để trống");
                        break;
                    case "sex":
                        e.target.setCustomValidity("Giới tính: Không được để trống");
                        break;  
                    case "inp_nationality":
                        e.target.setCustomValidity("Quốc tịch: Không được để trống");
                        break; 
                    case "inp_birthday":
                        e.target.setCustomValidity("Ngày sinh: Không được để trống");
                        break; 
                    case "inp_birthplace":
                        e.target.setCustomValidity("Nơi sinh: Không được để trống");
                        break; 
                    case "inp_cmtnd":
                        e.target.setCustomValidity("Số CMTND/CCCD: Không được để trống");
                        break;
                    case "inp_cmtnddate":
                        e.target.setCustomValidity("Ngày cấp CMTND/CCCD: Không được để trống");
                        break;
                    case "inp_cmtndplace":
                        e.target.setCustomValidity("Nơi cấp CMTND/CCCD: Không được để trống");
                        break;
                    case "inp_passport":
                        e.target.setCustomValidity("Số hộ chiếu: Không được để trống");
                        break;
                    case "inp_passport_expire_date":
                        e.target.setCustomValidity("Ngày hết hạn hộ chiếu: Không được để trống");
                        break;
                    case "inp_email_private":
                        e.target.setCustomValidity("Email cá nhân: Không được để trống");
                        break;                        
                    case "inp_tax_num":
                        e.target.setCustomValidity("Mã số thuế: Không được để trống");
                        break;       
                    case "inp_ensurance_num":
                        e.target.setCustomValidity("Số sổ bảo hiểm: Không được để trống");
                        break;    
                    case "inp_bank_num":
                        e.target.setCustomValidity("Tài khoản ngân hàng: Không được để trống");
                        break;    
                    case "inp_bank_name":
                        e.target.setCustomValidity("Tên ngân hàng: Không được để trống");
                        break;              
                }
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
    var elements = document.getElementsByTagName("SELECT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                switch (e.srcElement.id) {
                    case "BitrixID":
                        e.target.setCustomValidity("Chọn một tài khoản Bitrix");
                        break;
                }
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
// Onloading: Check nationality
// if=vn, remove required for passport num, passportdate. Else disable CMTND,...
    var varNation = $("#inp_nationality").val();
    var new_national=xoa_dau(varNation);
    var tmp_national=new_national.replace(/\s/,'');
    tmp_national=tmp_national.toUpperCase();
    if((tmp_national.trim() == 'VIETNAM') && (tmp_national.trim() != ''))
    {
        //Enable: CMTND,NgayCap,NoiCap 
        $("#inp_cmtnd").prop('required',true);
        $("#inp_cmtnd").prop('disabled', false);
        
        $("#inp_cmtnddate").prop('required',true);
        $("#inp_cmtnddate").prop('disabled', false);

        $("#inp_cmtndplace").prop('required',true);
        $("#inp_cmtndplace").prop('disabled', false);

        $("#lbl_cmtnd").show();
        $("#lbl_cmtnddate").show();
        $("#lbl_cmtndpalce").show();

        // Enable && not Required PASSPORT, PASSPORT EXPRIED DATE
        $("#inp_passport").prop('required',false);
        $("#inp_passport").prop('disabled', false);
        
        $("#inp_passport_expire_date").prop('required', false);
        $("#inp_passport_expire_date").prop('disabled', false);

        $("#lbl_required_passport").hide();
        $("#lbl_required_passport_date").hide();
    }
    if((tmp_national.trim() != 'VIETNAM') && (tmp_national.trim() != '')) //Disable: CMTND,NgayCap,NoiCap 
    {
        $("#inp_cmtnd").val('');
        $("#inp_cmtnd").removeAttr('required');
        $("#inp_cmtnd").prop('disabled', true);

        $("#inp_cmtnddate").val('');
        $("#inp_cmtnddate").removeAttr('required');
        $("#inp_cmtnddate").prop('disabled', true);

        $("#inp_cmtndplace").val('');
        $("#inp_cmtndplace").removeAttr('required');
        $("#inp_cmtndplace").prop('disabled', true);

        $("#lbl_required_passport").show();
        $("#lbl_required_passport_date").show();

        $("#lbl_cmtnd").hide();
        $("#lbl_cmtnddate").hide();
        $("#lbl_cmtndpalce").hide();

        //&& Endable PASSPORT, PASSPORT EXPRIED DATE
        $("#inp_passport").prop('required',true);
        $("#inp_passport").prop('disabled', false);
        
        $("#inp_passport_expire_date").prop('required', true);
        $("#inp_passport_expire_date").prop('disabled', false);

        $("#lbl_required_passport").show();
        $("#lbl_required_passport_date").show();
    }
///////////////////////////////////////////////////////////////////////////////////
  $("form").submit(function (e) {
       if (validationFailed) {
          e.preventDefault();
          return false;
       }
    }); 

    $(".chosen-select").chosenImage({
        width: '100%',
        no_results_text: "Không tìm thấy :"
    });
    $("#btn_restore_img").click(function(){
        // RestoreImage();
        var imgurl=$("#hidden_inp").val();
        $("#img_avatar").attr('src',imgurl);
        $('#fileToUpload').val("");
        $(this).prop('disabled', true);
    });
    $("#fileToUpload").change(function(){
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            $('#fileToUpload').val('');
            alert("Chỉ chấp nhận các file định dạng : "+fileExtension.join(', '));
        }
        else if (this.files[0].size>1200000) {
            alert("File quá lớn: "+ this.files[0].size+". Chỉ chấp nhận kích thước dưới 1,2M");
            $('#fileToUpload').val("");
        } 
          else
          {
            readURL(this);
            $("#btn_restore_img").prop('disabled', false);
          }
    });

    $("#img_avatar").click(function(){
        $("#fileToUpload").trigger('click');

    });
    $("#img_avatar").bind('load', function() {
         if($('#fileToUpload').val()!=""){
            $("#btn_restore_img").show(); 
         }
    });
////////////////  VALIDATE INPUT ////////////////////////////////
    $('#inp_staff_code').change(function(){
        var cString = $("#inp_staff_code").val();
        var patt= new RegExp(/^[a-z0-9]+$/i);

        if ( patt.test(cString))
         {
            validationFailed=false;
            $('#inp_staff_code').removeClass("has-alert");
            $("#staff_code_error").text('');
            $("#staff_code_error").hide();
            var urlpath=$(location).attr('pathname');
            var i= urlpath.indexOf('/edit');
            var varID=urlpath.charAt(i-1);
            $.ajax({
                url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
                type: 'post',
                dataType: "json",
                data: {
                search: cString, CurrentID:varID, fieldname:'CHECK_STAFF_CODE' 
                   },
            success: function( data ) {
                    var arr_check=data;
                if (arr_check.length >0)
                    {
                    validationFailed=true;
                    $("#staff_code_error").text('Trùng mã nhân viên');
                    $("#staff_code_error").show();
                    $("#inp_staff_code").focus();
                    }
                }
            });
        }
        else
        {
            $('#inp_staff_code').addClass("has-alert");
            $("#staff_code_error").text('Nhập lại mã nhân viên');
            $("#staff_code_error").show();
        }
    }); 
    $('#inp_full_name').change(function(){
        var   cString = $("#inp_full_name").val();
        var patt= new RegExp(str_patt);
        if ( patt.test(cString))
         {
            $('#inp_full_name').removeClass("has-alert");
            $("#name_error").text('');
            $("#name_error").hide();
        }
        else
        {
            $('#inp_full_name').addClass("has-alert");
            $("#name_error").text('Nhập lại tên nhân viên');
            $("#name_error").show();
        }
    }); 
    $('#sex').change(function(){
        var   cString = $("#sex").val();
        var patt= new RegExp(str_patt);
        if ( patt.test(cString))
         {
            $('#sex').removeClass("has-alert");
            $("#sex_error").text('');
            $("#sex_error").hide();
        }
        else
        {
            $('#sex').addClass("has-alert");
            $("#sex_error").text('Nhập lại giới tính');
            $("#sex_error").show();
        }
    }); 

    $('#inp_nationality').change(function(){
        var   cString = $("#inp_nationality").val();
        var patt= new RegExp(str_patt);
        if ( patt.test(cString))
         {
            $('#inp_nationality').removeClass("has-alert");
            $("#nationality_error").text('');
            $("#nationality_error").hide();

            var new_national=xoa_dau(cString);
            var tmp_national=new_national.replace(/\s/,'');
            tmp_national=tmp_national.toUpperCase();
            if((tmp_national.trim() == 'VIETNAM'))
            {
                //Enable: CMTND,NgayCap,NoiCap 
                $("#inp_cmtnd").prop('required',true);
                $("#inp_cmtnd").prop('disabled', false);
                
                $("#inp_cmtnddate").prop('required',true);
                $("#inp_cmtnddate").prop('disabled', false);

                $("#inp_cmtndplace").prop('required',true);
                $("#inp_cmtndplace").prop('disabled', false);

                $("#lbl_cmtnd").show();
                $("#lbl_cmtnddate").show();
                $("#lbl_cmtndpalce").show();

                // Enable && not Required PASSPORT, PASSPORT EXPRIED DATE
                $("#inp_passport").prop('required',false);
                $("#inp_passport").prop('disabled', false);
                
                $("#inp_passport_expire_date").prop('required', false);
                $("#inp_passport_expire_date").prop('disabled', false);

                $("#lbl_required_passport").hide();
                $("#lbl_required_passport_date").hide();
            }
            else //Disable: CMTND,NgayCap,NoiCap 
            {
                $("#inp_cmtnd").val('');
                $("#inp_cmtnd").removeAttr('required');
                $("#inp_cmtnd").prop('disabled', true);

                $("#inp_cmtnddate").val('');
                $("#inp_cmtnddate").removeAttr('required');
                $("#inp_cmtnddate").prop('disabled', true);

                $("#inp_cmtndplace").val('');
                $("#inp_cmtndplace").removeAttr('required');
                $("#inp_cmtndplace").prop('disabled', true);

                $("#lbl_required_passport").show();
                $("#lbl_required_passport_date").show();

                $("#lbl_cmtnd").hide();
                $("#lbl_cmtnddate").hide();
                $("#lbl_cmtndpalce").hide();

                //&& Endable PASSPORT, PASSPORT EXPRIED DATE
                $("#inp_passport").prop('required',true);
                $("#inp_passport").prop('disabled', false);
                
                $("#inp_passport_expire_date").prop('required', true);
                $("#inp_passport_expire_date").prop('disabled', false);

                $("#lbl_required_passport").show();
                $("#lbl_required_passport_date").show();
            }
        }
        else
        {
            $('#inp_nationality').addClass("has-alert");
            $("#nationality_error").text('Nhập lại quốc tịch');
            $("#nationality_error").show();
        }
    }); 

    $('#inp_minority').change(function(){
        var   cString = $("#inp_minority").val();
        var patt= new RegExp(str_patt);
        validationFailed=false;
        if (cString.trim()!="")
        { 
          if ( patt.test(cString))
           {
              $('#inp_minority').removeClass("has-alert");
              $("#minority_error").text('');
              $("#minority_error").hide();
          }
          else
          {
              $('#inp_minority').addClass("has-alert");
              $("#minority_error").text('Nhập lại dân tộc');
              $("#minority_error").show();
              validationFailed=true;
              $('#inp_minority').focus();

          }
        }
    }); 

    $('#inp_religion').change(function(){
        var   cString = $("#inp_religion").val();
        var patt= new RegExp(str_patt);
        validationFailed=false;
        if (cString.trim()!="")
        { 
          if ( patt.test(cString))
           {
              $('#inp_religion').removeClass("has-alert");
              $("#religion_error").text('');
              $("#religion_error").hide();
          }
          else
          {
              $('#inp_religion').addClass("has-alert");
              $("#religion_error").text('Nhập lại tôn giáo');
              $("#religion_error").show();
              validationFailed=true;
              $('#inp_religion').focus();

          }
        }
    }); 

    $('#inp_birthday').change(function(){
        var   cString = $("#inp_birthday").val();
        if (cString!="")
         {
            $('#birthday_error').removeClass("has-alert");
            $("#birthday_error").text('');
            $("#birthday_error").hide();
        }
        else
        {
            $('#inp_nationality').addClass("has-alert");
            $("#birthday_error").text('Nhập lại ngày sinh');
            $("#birthday_error").show();
        }
    }); 

    $('#inp_birthplace').change(function(){
        var   cString = $("#inp_birthplace").val();
        var patt= new RegExp(str_patt);
        if ( patt.test(cString))
         {
            $('#inp_birthplace').removeClass("has-alert");
            $("#birthplace_error").text('');
            $("#birthplace_error").hide();
        }
        else
        {
            $('#inp_birthplace').addClass("has-alert");
            $("#birthplace_error").text('Nhập lại nơi sinh');
            $("#birthplace_error").show();
        }
    }); 

    $('#inp_cmtnd').change(function(){
      var nationa_val=$("#inp_nationality").val();
      var new_national=xoa_dau(nationa_val);
      new_national=new_national.replace(/\s/,'');
      new_national=new_national.toUpperCase();
      if(new_national=="VIETNAM" || new_national=="VIET" )
        {
            var   cString = $("#inp_cmtnd").val();
            var patt= new RegExp(str_patt_num);
            $("#inp_cmtnd").prop('required',true);
            if ( patt.test(cString))
             {
                $('#inp_cmtnd').removeClass("has-alert");
                $("#cmtnd_error").text('');
                $("#cmtnd_error").hide();
                var url_path=$(location).attr('pathname');
                var j= url_path.indexOf('/edit');
                var varCurID=url_path.charAt(j-1);
                // Check duplicate CMTND number
                $.ajax({
                    url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                    search: cString, CurrentID:varCurID, fieldname:'CHECK_CMTND' 
                          },
                success: function( data ) {
                        var arr_check=data;
                    if (arr_check.length >0)
                        {
                          validationFailed=true;
                          $("#cmtnd_error").text('Trùng CMTND/CCCD');
                          $("#cmtnd_error").show();
                          $("#inp_cmtnd").focus();
                        }
                    }
                });
            }
            else
            {
                $('#inp_cmtnd').addClass("has-alert");
                $("#cmtnd_error").text('Nhập lại CMTND/CCCD');
                $("#cmtnd_error").show();
            }
       }
       else
       {   $("#inp_cmtnd").removeAttr('required');} 
    }); 

  $('#inp_cmtnddate').change(function(){
    var nationa_val=$("#inp_nationality").val();
    var new_national=xoa_dau(nationa_val);
    new_national=new_national.replace(/\s/,'');
    new_national=new_national.toUpperCase();
    if(new_national=="VIETNAM" || new_national=="VIET" )
        {
          var cString = $("#inp_cmtnddate").val();
          $("#inp_cmtnddate").prop('required',true);
          if ( cString!="")
           {
              $('#inp_cmtnddate').removeClass("has-alert");
              $("#cmtndplace_error").text('');
              $("#cmtndplace_error").hide();
          }
          else
          {
              $('#inp_cmtnddate').addClass("has-alert");
              $("#cmtndplace_error").text('Nhập nơi cấp');
              $("#cmtndplace_error").show();
          }
        }
        else {
              $("#inp_cmtnddate").removeAttr('required');
        }
    });

    $('#inp_cmtndplace').change(function(){
      var nationa_val=$("#inp_nationality").val();
      var new_national=xoa_dau(nationa_val);
      new_national=new_national.replace(/\s/,'');
      new_national=new_national.toUpperCase();
      if(new_national=="VIETNAM" || new_national=="VIET" )
        {
            var   cString = $("#inp_cmtndplace").val();
            var patt= new RegExp(str_patt);
            $("#inp_cmtndplace").prop('required',true);
            if ( patt.test(cString))
             {
                $('#inp_cmtndplace').removeClass("has-alert");
                $("#cmtndplace_error").text('');
                $("#cmtndplace_error").hide();
            }
            else
            {
                $('#inp_cmtndplace').addClass("has-alert");
                $("#cmtndplace_error").text('Nhập lại CMTND/CCCD');
                $("#cmtndplace_error").show();
            }
       }
       else
       {   $("#inp_cmtndplace").removeAttr('required');} 
    }); 


    $('#inp_passport').change(function(){
      var nationa_val=$("#inp_nationality").val();
      var new_national=xoa_dau(nationa_val);
      new_national=new_national.replace(/\s/,'');
      new_national=new_national.toUpperCase();
      var   cString = $("#inp_passport").val();
      var patt= new RegExp(str_patt_num);
      
      if(!(new_national=="VIETNAM" || new_national=="VIET"))
        {
            $("#lbl_required_passport").show();
            $("#lbl_required_passport_date").show();
            if ( patt.test(cString))
              {
                $('#inp_passport').removeClass("has-alert");
                $("#passport_error").text('');
                $("#passport_error").hide();
              }
            else
              {
                $('#inp_passport').addClass("has-alert");
                $("#passport_error").text('Nhập lại số hộ chiếu');
                $("#passport_error").show();
              }
       }
       else
       { 
            // Passport not required
            $("#inp_passport").prop('required',false);
            $("#inp_passport").prop('required',false);
            $("#inp_passport").removeAttr('required');
            $("#lbl_required_passport").hide();
            $("#lbl_required_passport_date").hide();
            // But Passport have value => Check format
            if (cString!="")
            {
                  if ( patt.test(cString))
                  {
                    $('#inp_passport').removeClass("has-alert");
                    $("#passport_error").text('');
                    $("#passport_error").hide();
                  }
                else
                  {
                    $('#inp_passport').addClass("has-alert");
                    $("#passport_error").text('Nhập lại số hộ chiếu');
                    $("#passport_error").show();
                  }
            }
       } 
    }); 

  $('#inp_passport_expire_date').change(function(){
    var nationa_val=$("#inp_nationality").val();
    var new_national=xoa_dau(nationa_val);
    new_national=new_national.replace(/\s/,'');
    new_national=new_national.toUpperCase();
    if(!(new_national=="VIETNAM" || new_national=="VIET" ))
        {
          var cString = $("#inp_passport_expire_date").val();
          $("#inp_passport_expire_date").prop('required',true);
          if ( cString!="")
           {
              $('#inp_passport_expire_date').removeClass("has-alert");
              $("#passport_expire_date_error").text('');
              $("#passport_expire_date_error").hide();
          }
          else
          {
              $('#inp_passport_expire_date').addClass("has-alert");
              $("#passport_expire_date_error").text('Nhập ngày hết hạn');
              $("#passport_expire_date_error").show();
          }
        }
        else { $("#inp_passport_expire_date").removeAttr('required');
        }
    });

      $('#inp_phone_num').change(function(){
        var   cString = $("#inp_phone_num").val();
        var patt= new RegExp(str_patt_num);
        if ( patt.test(cString))
         {
            $('#inp_phone_num').removeClass("has-alert");
            $("#phone_num_error").text('');
            $("#phone_num_error").hide();
        }
        else
        {
            $('#inp_phone_num').addClass("has-alert");
            $("#phone_num_error").text('Nhập lại số điện thoại');
            $("#phone_num_error").show();
        }
      }); 

      $('#inp_email_private').change(function(){
        var   cString = $("#inp_email_private").val();
        var patt= new RegExp(str_patt_email);
        if ( patt.test(cString))
         {
            $('#inp_email_private').removeClass("has-alert");
            $("#email_private_error").text('');
            $("#email_private_error").hide();
        }
        else
        {
            $('#inp_email_private').addClass("has-alert");
            $("#email_private_error").text('Nhập lại email');
            $("#email_private_error").show();
        }
      }); 
      $('#inp_tax_num').change(function(){
        var   cString = $("#inp_tax_num").val();
        var patt= new RegExp(str_patt_num);
        if ( patt.test(cString))
         {
            $('#inp_tax_num').removeClass("has-alert");
            $("#tax_num_error").text('');
            $("#tax_num_error").hide();
        }
        else
        {
            $('#inp_tax_num').addClass("has-alert");
            $("#tax_num_error").text('Nhập MST');
            $("#tax_num_error").show();
        }
      }); 
      $('#inp_ensurance_num').change(function(){
        var   cString = $("#inp_ensurance_num").val();
        var patt= new RegExp(str_patt_num);
        if ( patt.test(cString))
         {
            $('#inp_ensurance_num').removeClass("has-alert");
            $("#ensurance_num_error").text('');
            $("#ensurance_num_error").hide();
        }
        else
        {
            $('#inp_ensurance_num').addClass("has-alert");
            $("#ensurance_num_error").text('Nhập số bảo hiểm');
            $("#ensurance_num_error").show();
        }
      });
      $('#inp_bank_num').change(function(){
        var   cString = $("#inp_bank_num").val();
        var patt= new RegExp(str_patt_num);
        if ( patt.test(cString))
         {
            $('#inp_bank_num').removeClass("has-alert");
            $("#bank_num_error").text('');
            $("#bank_num_error").hide();
        }
        else
        {
            $('#inp_bank_num').addClass("has-alert");
            $("#bank_num_error").text('Nhập số tài khoản');
            $("#bank_num_error").show();
        }
      });
      $('#inp_bank_name').change(function(){
        var   cString = $("#inp_bank_name").val();
        var patt= new RegExp(str_patt_add);
        if ( patt.test(cString))
         {
            $('#inp_bank_name').removeClass("has-alert");
            $("#bank_name_error").text('');
            $("#bank_name_error").hide();
        }
        else
        {
            $('#inp_bank_name').addClass("has-alert");
            $("#bank_name_error").text('Nhập tên ngân hàng');
            $("#bank_name_error").show();
        }
      });

      $('#inp_first_date').change(function(){
        var   cString = $("#inp_first_date").val();
        if ( cString.trim()!="")
         {
            $('#inp_first_date').removeClass("has-alert");
            $("#first_date_error").text('');
            $("#first_date_error").hide();
        }
        else
        {
            $('#inp_first_date').addClass("has-alert");
            $("#first_date_error").text('Nhập ngày vào công ty');
            $("#first_date_error").show();
        }
      });

      $('#inp_email_company').change(function(){
        var   cString = $("#inp_email_company").val();
        var patt= new RegExp(str_patt_email);
        if ( patt.test(cString))
         {
            $('#inp_email_company').removeClass("has-alert");
            $("#email_company_error").text('');
            $("#email_company_error").hide();
        }
        else
        {
            $('#inp_email_company').addClass("has-alert");
            $("#email_company_error").text('Nhập email công ty');
            $("#email_company_error").show();
        }
      });
    $('#inp_staff_old_code').change(function(){
        var   cString = $("#inp_staff_old_code").val();
        var patt= new RegExp(/^[a-z0-9]+$/i);
        validationFailed=false;
        if (cString.trim()!="")
        { 
          if ( patt.test(cString))
           {
              $('#inp_staff_old_code').removeClass("has-alert");
              $("#staff_old_code_error").text('');
              $("#staff_old_code_error").hide();
          }
          else
          {
              $('#inp_staff_old_code').addClass("has-alert");
              $("#staff_old_code_error").text('Nhập lại mã tham chiếu');
              $("#staff_old_code_error").show();
              validationFailed=true;
              $('#inp_staff_old_code').focus();

          }
        }
    }); 
      $('#inp_address_reg').change(function(){
        var   cString = $("#inp_address_reg").val();
        var patt= new RegExp(str_patt_add);
        if ( patt.test(cString))
         {
            $('#inp_address_reg').removeClass("has-alert");
            $("#address_reg_error").text('');
            $("#bank_name_error").hide();
        }
        else
        {
            $('#inp_address_reg').addClass("has-alert");
            $("#address_reg_error").text('Nhập địa chỉ');
            $("#address_reg_error").show();
        }
      });

      $('#inp_address').change(function(){
        var   cString = $("#inp_address").val();
        var patt= new RegExp(str_patt_add);
        if ( patt.test(cString))
         {
            $('#inp_address').removeClass("has-alert");
            $("#address_error").text('');
            $("#bank_name_error").hide();
        }
        else
        {
            $('#inp_address').addClass("has-alert");
            $("#address_error").text('Nhập địa chỉ nơi ở');
            $("#address_error").show();
        }
      });

      $('#inp_reference_name').change(function(){
        var   cString = $("#inp_reference_name").val();
        var patt= new RegExp(str_patt);
        if ( patt.test(cString))
         {
            $('#inp_reference_name').removeClass("has-alert");
            $("#reference_name_error").text('');
            $("#reference_name_error").hide();
        }
        else
        {
            $('#inp_reference_name').addClass("has-alert");
            $("#reference_name_error").text('Nhập tên');
            $("#reference_name_error").show();
        }
      });


    $('#inp_reference_address').change(function(){
        var   cString = $("#inp_reference_address").val();
        var patt= new RegExp(str_patt_add);
        validationFailed=false;
        if (cString.trim()!="")
        { 
          if ( patt.test(cString))
           {
              $('#inp_reference_address').removeClass("has-alert");
              $("#reference_address_error").text('');
              $("#reference_address_error").hide();
          }
          else
          {
              $('#inp_reference_address').addClass("has-alert");
              $("#reference_address_error").text('Nhập lại địa chỉ');
              $("#reference_address_error").show();
              validationFailed=true;
              $('#inp_reference_address').focus();

          }
        }
    }); 
       $('#inp_reference_phone').change(function(){
        var   cString = $("#inp_reference_phone").val();
        var patt= new RegExp(str_patt_num);
        if ( patt.test(cString))
         {
            $('#inp_reference_phone').removeClass("has-alert");
            $("#reference_phone_error").text('');
            $("#reference_phone_error").hide();
        }
        else
        {
            $('#inp_reference_phone').addClass("has-alert");
            $("#reference_phone_error").text('Nhập lại số điện thoại');
            $("#reference_phone_error").show();
        }
      }); 

    $('#inp_reference_email').change(function(){
        var   cString = $("#inp_reference_email").val();
        var patt= new RegExp(str_patt_email);
        validationFailed=false;
        if (cString.trim()!="")
        { 
          if ( patt.test(cString))
           {
              $('#inp_reference_email').removeClass("has-alert");
              $("#reference_email_error").text('');
              $("#reference_email_error").hide();
          }
          else
          {
              $('#inp_reference_email').addClass("has-alert");
              $("#reference_email_error").text('Nhập lại email');
              $("#reference_email_error").show();
              validationFailed=true;
              $('#inp_reference_email').focus();

          }
        }
    }); 

    //END OF VALIDATE INPUT///////////////////////////////////////////////////
    //START AUTO COMPLETE
  $( "#sex" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'post',
          dataType: "json",
          data: {
           search: request.term, fieldname:'SEX' //fieldname:exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#sex').val(ui.item.label); // display the selected text
           // $('#sex').val(ui.item.value); // save selected id to input
           return false;
    }
    });
  $( "#inp_minority" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'post',
          dataType: "json",
          data: {
           search: request.term, fieldname:'MINORITY' //fieldname:exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#inp_minority').val(ui.item.label); // display the selected text
           // $('#inp_minority').val(ui.item.value); // save selected id to input
           return false;
    }
    });
  $( "#inp_nationality" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'Post',
          dataType: "json",
          data: {
           search: request.term,fieldname: 'NATIONALITY' //fieldname: exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#inp_nationality').val(ui.item.label); // display the selected text
           // $('#inp_nationality').val(ui.item.value); // save selected id to input
           return false;
    }
    });

  $( "#inp_religion" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'Post',
          dataType: "json",
          data: {
           search: request.term,fieldname: 'RELIGION' //ajax_action: exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#inp_religion').val(ui.item.label); // display the selected text
           // $('#inp_religion').val(ui.item.value); // save selected id to input
           return false;
    }
    });
  $( "#inp_birthplace" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'Post',
          dataType: "json",
          data: {
           search: request.term,fieldname: 'PLACE_OF_BIRTH' //fieldname: exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#inp_birthplace').val(ui.item.label); // display the selected text
           // $('#inp_birthplace').val(ui.item.value); // save selected id to input
           return false;
    }
    });
  $( "#inp_cmtndplace" ).autocomplete({
      source: function( request, response ) {
          // Fetch data
          $.ajax({
          url: "/local/components/ngs.hr/assets/ajax_lib/employee_crud.php",
          type: 'Post',
          dataType: "json",
          data: {
           search: request.term,fieldname: 'CMTND_PLACE' //fieldname: exactly fieldname in HR_EMPLOYEE
          },
              success: function( data ) {
                    response( data );
              }
          });
    },
          select: function (event, ui) {
           // Set selection
           $('#inp_cmtndplace').val(ui.item.label); // display the selected text
           // $('#inp_cmtndplace').val(ui.item.value); // save selected id to input
           return false;
    }
    });
});
/////////////////////////////////////////////////////////////////////////////

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#img_avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
function RestoreImage()
{
    var imgurl=$("#hidden_inp").val();
    $("#img_avatar").attr('src',imgurl);
    $('#fileToUpload').val("");
}

function xoa_dau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
}
    
