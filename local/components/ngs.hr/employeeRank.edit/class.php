<?php
defined('B_PROLOG_INCLUDED') || die;
use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Ngs\Hr\Entity\EmployeeTable;
use Ngs\Hr\Entity\EmployeeRankTable;
use Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;
use Bitrix\Main\Type;

class CNgsRankEditComponent extends CBitrixComponent
{
    
    const FORM_ID = 'EMPLOYEERANK_EDIT';
    private $errors;
    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);
        $this->errors = new ErrorCollection();
       // CBitrixComponent::includeComponentClass('ngs.hr:employees.list');
    }  
    public function executeComponent()
    {

        global $APPLICATION,$USER;
        // $title = Loc::getMessage('CRMSTORES_SHOW_TITLE_DEFAULT');
        $title="Editing employee";
        if (!Loader::includeModule('ngs.hr')) {
            ShowError(('CRMSTORES_NO_MODULE'));
            //echo "string";die;
            return;
        }

        $employee = array(
            'ID' => '',
            'FULL_NAME' => '',
            'STATUS' => '',
            'AVATAR' => '',
           
        );
       
        if (intval($this->arParams['ID']) > 0) {
            $dbEmployee = EmployeeRankTable::getById($this->arParams['ID']);
            $employee = $dbEmployee->fetch();

            if (empty($employee)) {
                ShowError(Loc::getMessage('HR_EMPLOYEE_NOT_FOUND'));
                return;
            }
        } elseif (intval($this->arParams['ID']) == 0)
        {

        }
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        if (intval($employee['ID']) > 0) {
                $title = Loc::getMessage(
                    'HR_SHOW_TITLE',
                    array(
                        '#ID#' => $employee['ID'],
                        '#NAME#' => $employee['FULL_NAME']
                    )
                );
                $new_avatar=$employee['AVATAR'];
            }
            else
            {
                //$title='Thêm mới nhân viên';
                $title = Loc::getMessage('HR_SHOW_TITLE_ADD');
                $new_avatar=null;   
            }
        $APPLICATION->SetTitle($title);
        if ($request->isPost()) {
            $this->errors=$this->validate($employee['ID'],$request->get('staff_code'));
            $empid=$employee['ID'];
            if ($this->errors->isEmpty())
            {
                switch ($request["btnsubmit"]) {
                    case 'save':
                            if ($employee['ID']!=0)
                            {
                            $submittedval= $this::getFormSubmitted();
                            $this->errors=$this->ProccessSave($submittedval,$employee['ID'],$employee['STAFF_CODE']);
                            LocalRedirect(CComponentEngine::makePathFromTemplate($this->arParams['URL_TEMPLATES']['EDIT'], array('ID' => $employee['ID'])));

                            }
                            if ($employee['ID']==0)
                            {
                            $submittedval= $this::getFormSubmitted();
                            $newid=$this->ProccessSaveNew($submittedval,$employee['STAFF_CODE']);
                            LocalRedirect(CComponentEngine::makePathFromTemplate($this->arParams['URL_TEMPLATES']['EDIT'], array('ID' => $newid)));
                            }                            
                        break;
                    case 'saveadd':
                        {
                            // SAVE
                            if ($employee['ID']!=0)
                            {
                            $submittedval= $this::getFormSubmitted();
                            $this->ProccessSave($submittedval,$employee['ID'],$employee['STAFF_CODE']);
                            }
                            // Redirect id=0 to add new employee
                            $employee=[];
                            $employee['ID']=0;
                            LocalRedirect(CComponentEngine::makePathFromTemplate($this->arParams['URL_TEMPLATES']['EDIT'], array('ID' =>$employee['ID'])));
                        }
                        break; 
                    case 'cancel':
                        {
                            LocalRedirect('/nhan-su/');
                        break;
                        }                   
                    default:
                        # code...
                        break;
                    }
            }
        }
        // Getting list of NIS account
        $approveMember = UserTable::getList(array('filter' => ['ACTIVE' => 'Y','!=ID'=>$memberExtranet]))->fetchAll();
        $memberArr = [];
        foreach ($approveMember as $arUser) {
            $userImage = \CFile::resizeImageGet(
                $arUser['PERSONAL_PHOTO'], array('width' => 38, 'height' => 38),
                BX_RESIZE_IMAGE_EXACT, false
            );
            $memberArr[] = [
                'ID' => $arUser['ID'],
                'IMAGE' => !empty($userImage['src']) ? $userImage['src'] : '/local/components/ngs.hr/assets/theme/images/default-non-user-no-photo-1.jpg',
                'NAME' => \CUser::formatName(\CSite::getNameFormat(), $arUser, true, false)
            ];
        }
        // var_dump($this->errors);
        //End of Getting list of NIS account
        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            //'GRID_ID' => CNgsHREditComponent::GRID_ID,
            'TITLE' => $title,
            'EMPLOYEE' => $employee,
            'memberArr' => $memberArr,
            'NEW_AVATAR' =>$new_avatar,
            'ImgNotice' =>$img_lable_notice,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        );
        $this->includeComponentTemplate();
    }
    private function getRedirectUrl($savedStoreId = null)
    {
            // $context = Context::getCurrent();
            // $request = $context->getRequest();

            // if (!empty($savedStoreId) && $request->offsetExists('apply')) {
            //     return CComponentEngine::makePathFromTemplate(
            //         $this->arParams['URL_TEMPLATES']['EDIT'],
            //         array('SOLUTION_ID' => $savedStoreId)
            //     );
            // } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
            //     return CComponentEngine::makePathFromTemplate(
            //         $this->arParams['URL_TEMPLATES']['EDIT'],
            //         array('SOLUTION_ID' => 0)
            //     );
            // }

            // $backUrl = $request->get('backurl');
            // if (!empty($backUrl)) {
            //     return $backUrl;
            // }

            // if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
            //     return CComponentEngine::makePathFromTemplate(
            //         $this->arParams['URL_TEMPLATES']['DETAIL'],
            //         array('SOLUTION_ID' => $savedStoreId)
            //     );
            // } else {
            //     return $this->arParams['SEF_FOLDER'];
            // }
            return "empty url";
    }
private  function getFormSubmitted()
{
    global $DB;

    $request = \Bitrix\Main\Context::getCurrent()->getRequest();

    $arEmployee=[
        'STAFF_CODE' =>"'".$request->get('staff_code')."'",
        'STATUS' =>"'".$request->get('status')."'",
        'FULL_NAME' =>"'".$request->get('full_name')."'",
        // 'AVATAR' =>"'".$request->get('hidden_inp')."'",
        'SEX' =>"'".$request->get('sex')."'",
        'MINORITY' =>"'".$request->get('minority')."'",
        'MARRIAGE' =>"'".$request->get('marriage')."'",
        'NATIONALITY' =>"'".$request->get('nationality')."'",
        'RELIGION' =>"'".$request->get('religion')."'",
        'DATE_OF_BIRTH' =>"'". $this->wellformatdate($request->get('birthday')) ."'",
        'PLACE_OF_BIRTH' =>"'".$request->get('birthplace')."'",
        'CMTND' =>"'".$request->get('cmtnd')."'",
        'CMTND_DATE' =>"'".$this->wellformatdate($request->get('cmtnddate'))."'",
        'CMTND_PLACE' =>"'".$request->get('cmtndplace')."'",
        'PASSPORT' =>"'".$request->get('passport')."'",
        'PASSPORT_EXPIRE_DATE' =>"'".$this->wellformatdate($request->get('passport_expire_date'))."'",
        'PHONE_NUM' =>"'".$request->get('phone_num')."'",
        'EMAIL_PRIVATE' =>"'".$request->get('email_private')."'",
        'TAX_NUM' =>"'".$request->get('tax_num')."'",
        'ENSURANCE_NUM' =>"'".$request->get('ensurance_num')."'",
        'BANK_NUM' =>"'".$request->get('bank_num')."'",
        'BANK_NAME' =>"'".$request->get('bank_name')."'",
        'FIRST_DATE' =>"'".$this->wellformatdate($request->get('first_date'))."'",
        'EMAIL_COMPANY' =>"'".$request->get('email_company')."'",
        'STAFF_OLD_CODE' =>"'".$request->get('staff_old_code')."'",
        'BITRIX_ID' =>"'".$request->get('BitrixID')."'",
        'ADDRESS' =>"'".$request->get('address')."'",
        'ADDRESS_REG' =>"'".$request->get('address_reg')."'",
        'REFERENCE_NAME' =>"'".$request->get('reference_name')."'",
        'REFERENCE_PHONE' =>"'".$request->get('reference_phone')."'",
        'REFERENCE_EMAIL' =>"'".$request->get('reference_email')."'",
        'REFERENCE_ADDRESS' =>"'".$request->get('reference_address')."'",
    ];
    return $arEmployee;
}
private  function ProccessSave($submited_emp,$EMPLOYEE_ID,$var_staff_code)
{
    global $DB;
    $save_error = new ErrorCollection();
    $file_name='';
    
    // $DB->StartTransaction();
        $DB->Update(EmployeeTable::getTableName(),$submited_emp,'WHERE ID='.$EMPLOYEE_ID,$Err_mess.__LINE__);
        
        if (!empty($Err_mess) || $Err_mess ==null)
        {
            // $DB->Commit();
            // $targetfile = basename($_FILES["fileToUp"]["name"]);
            $file_name = basename($_FILES["fileToUp"]["name"]);
            if(strlen($file_name)>0)
            {
                $upload_result =$this->upload_image($EMPLOYEE_ID,$var_staff_code);
                if($upload_result > 0)
                {
                    $save_error->setError(new Error($upload_result));
                    return $save_error;
                }
                else
                {
                    $imgurl="'\\\\upload_hr\\\\".$EMPLOYEE_ID .'\\\\' .$file_name."'";
                    $DB->Update(EmployeeTable::getTableName(),array('AVATAR' =>$imgurl),'WHERE ID='.$EMPLOYEE_ID);
                }
            }
        }
        else
        {
            // $DB->Rollback();            
            $save_error->setError(new Error('Không thể cập nhật thông tin nhân viên(Can not update).'));
            return $save_error;
        }
}
private  function ProccessSaveNew($submited_emp,$var_staff_code)
{
    global $DB;
    $addnew_error = new ErrorCollection();
    $DB->StartTransaction();
    $id_inserted=$DB->Insert(EmployeeTable::getTableName(),$submited_emp,$ErrInsert_mess.__LINE__);
    $id_inserted=intval($id_inserted);
    if (strlen($ErrInsert_mess)<=0)
    {
        $DB->Commit();  
        $target_file = basename($_FILES["fileToUp"]["name"]);   

         if (!empty($target_file) || $target_file ==null)
        {
            $addnew_error =$this->upload_image($id_inserted,$var_staff_code);
            if($addnew_error > 0)
            {
                return -1;
            }
            else
            {
                $imgurl="'\\\\upload_hr\\\\".$id_inserted.'\\\\' .basename( $_FILES["fileToUp"]["name"])."'";
                $DB->Update(EmployeeTable::getTableName(),array('AVATAR' =>$imgurl),'WHERE ID='.$id_inserted);
            }
        }
        return $id_inserted; 
    }
    else
    {
        $DB->Rollback();          
        $addnew_error->setError(new Error('Không thể tạo mới nhân viên(Can not insert).'));
        return 0;
    }
}
private function wellformatdate($tempdate)
{
    if ($tempdate!=null)
    {
        $tempdate_format=explode('/',$tempdate);
        $tempdate_wellformat=$tempdate_format[2].'-'.$tempdate_format[0].'-'.$tempdate_format[1];
        return $tempdate_wellformat;
    }
    else{return null;}
}

private function validate($varID,$staffcode)
{
    $Loi = new ErrorCollection();
    $target_dir= $_SERVER['DOCUMENT_ROOT'].'upload_hr/'.$varID.'/';
    $file_name = basename($_FILES["fileToUp"]["name"]);
    // var_dump('Validate Filename='.$file_name); die;
    $target_file = $target_dir . $file_name;
    if ($file_nameF !='')
        {
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
                $check = getimagesize($_FILES["fileToUp"]["tmp_name"]);
                if($check == false) {
                    $Loi->setError(new Error('File is not an image.'));
                    return $Loi;
                }
            // Check if file already exists
            if (file_exists($target_file)) {
                $Loi->setError(new Error('Sorry, file already exists.'));
                return $Loi;
            }
            // Check file size
            if ($_FILES["fileToUp"]["size"] > 1200000) {
                $Loi->setError(new Error('Sorry, your file is too large.'));
                return $Loi;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $Loi->setError(new Error('Sorry, only JPG, JPEG, PNG & GIF files are allowed.'));
                return $Loi;
            }
        } //if ($filename !='')
return $Loi;
}
}

