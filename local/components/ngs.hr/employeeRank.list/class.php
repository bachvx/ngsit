<?php
defined('B_PROLOG_INCLUDED') || die;

use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use Bitrix\Main\Grid;
//use Bitrix\Main\CForm;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;
use Ngs\Hr\Entity\EmployeeRankTable;
use Ngs\Hr\Entity\EmployeeTable;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;

class CEmployeeRankListComponent extends CBitrixComponent
{
    const GRID_ID = 'CRMSTORES_LIST';
    const SORTABLE_FIELDS = array();
    const FILTERABLE_FIELDS = array('UPPER_CODE', 'CODE', 'STATUS');
    const SUPPORTED_SERVICE_ACTIONS = array('GET_ROW_COUNT');
    const SUPPORTED_ACTIONS = array('delete');
    private static $headers;
    private static $filterFields;
    private static $filterPresets;

    public function __construct(CBitrixComponent $component = null)
    {
        global $DB;
        parent::__construct($component);
        $this->errors = new ErrorCollection();
        self::$headers = array(
            array(
                'id' => 'STT',
                'name' => 'STT',
                'sort' => 'STT',
                'first_order' => 'desc',
                'type' => 'int',
                'default' => true,
            ),
            array(
                'id' => 'UPPER_CODE',
                'name' => 'MÃ CẤP CÁN BỘ CHA',
                'default' => true,
            ),
            array(
                'id' => 'CODE',
                'name' => 'MÃ CẤP CÁN BỘ CON',
                'default' => true,
            ),
            array(
                'id' => 'STATUS',
                'name' => 'TRẠNG THÁI',
                'default' => true,
            )
        );
        $arrayLevel = array();
        $arrayLevel[''] = 'Chọn trạng thái';
        $arrayLevel['Có'] = 'Có';
        $arrayLevel['Không'] = 'Không';
        self::$filterFields = array(
            array(
                'id' => 'UPPER_CODE',
                'name' => 'MÃ CẤP CÁN BỘ CHA',
                'default' => true,
            ),
            array(
                'id' => 'CODE',
                'name' => 'MÃ CẤP CÁN BỘ CON',
                'default' => true,
            ),
            
            array(
                'id' => 'STATUS',
                'name' => 'TRẠNG THÁI',
                'default' => true,
                'type' => 'list',
                'params' => array(
                    'multiple' => 'N'
                ),
                'items' => $arrayLevel,
            ),
        );

        self::$filterPresets = array();
    }

    public function executeComponent()
    {
        if (!Loader::includeModule('ngs.hr')) {
            ShowError('Không tìm thấy module');
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();
//var_dump($context);
        $grid = new Grid\Options(self::GRID_ID);

        //region Sort
        $gridSort = $grid->getSorting();
        $sort = array_filter(
            $gridSort['sort'],
            function ($field) {
                return in_array($field, self::SORTABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        if (empty($sort)) {
//            $sort = array('STATUS' => 'asc');
        }
        //endregion

        //region Filter
        $gridFilter = new Filter\Options(self::GRID_ID, self::$filterPresets);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);
        $gridFilterValues = array_filter(
            $gridFilterValues,
            function ($fieldName) {
//                var_dump($fieldName);
                return in_array($fieldName, self::FILTERABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        //endregion
        if (empty($gridFilterValues['UPPER_CODE'])
            && empty($gridFilterValues['CODE'])
            && empty($gridFilterValues['STATUS'])) {
            $gridFilterValues = array();
        } else {
            $gridFilterValues['UPPER_CODE'] = empty($gridFilterValues['UPPER_CODE']) ? '' : $gridFilterValues['%=UPPER_CODE'] = '%' . $gridFilterValues['UPPER_CODE'] . '%';
            $gridFilterValues['CODE'] = empty($gridFilterValues['CODE']) ? '' : $gridFilterValues['CODE'] = $gridFilterValues['%=CODE'] = '%' . $gridFilterValues['CODE'] . '%';
           $gridFilterValues['STATUS'] = empty($gridFilterValues['STATUS']) ? '' : $gridFilterValues['STATUS'] = $gridFilterValues['%=STATUS'] = '%' . $gridFilterValues['STATUS'] . '%';
           
            unset($gridFilterValues['UPPER_CODE']);
            unset($gridFilterValues['CODE']);
            unset($gridFilterValues['STATUS']);
        }

      
        $this->processGridActions($gridFilterValues);
        $this->processServiceActions($gridFilterValues);

        //region Paginatio
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $pager->setRecordCount(EmployeeRankTable::getCount($gridFilterValues));
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }
        //endregion

        $stores = $this->getData(array(
            'filter' => $gridFilterValues,
            'limit' => $pager->getLimit(),
            'offset' => $pager->getOffset(),
            'order' => $sort
        ));

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        $this->arResult = array(
            'GRID_ID' => self::GRID_ID,
            'STORES' => $stores,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
            'SORT' => $sort,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
            'SERVICE_URL' => $requestUri->getUri(),
        );
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();

        if ($request->isPost()) {
            
            $this->errors=$this->validate($request->get('code'),$request->get('upper_code'));
           // $empid=$employee['ID'];
            //var_dump($this->errors->isEmpty());die;
            if ($this->errors->isEmpty())
            {
                switch ($request["btnsubmit"]) {
                    case 'save':
                            $newid=$this->ProccessSaveNew($request);   
                            LocalRedirect('/cap-bac-nhan-su');               
                        break;  
                    case 'update':  
                        $this->errors=$this->ProccessSave($request);
                        LocalRedirect('/cap-bac-nhan-su');   
                        break;                
                    default:
                        # code...
                        break;
                    }
            }
        }
        $this->includeComponentTemplate();
    }

    public function getData($params = array())
    {
        global $DB;
        //$err_mess = (CForm :: err_mess()). "<br> Function: GetByID <br> Line:" ;
      // var_dump($params);die;
        $dbStores = EmployeeRankTable::getList($params);
        $stores = $dbStores->fetchAll();
        //var_dump($stores);die;
        return $stores;
    }

    private function processGridActions($currentFilter)
    {
        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

            $action = $request->get('action_button_' . self::GRID_ID);

        if (!in_array($action, self::SUPPORTED_ACTIONS)) {
            return;
        }

        $allRows = $request->get('action_all_rows_' . self::GRID_ID) == 'Y';
        if ($allRows) {

            $dbStores = EmployeeRankTable::getList(array(
                'filter' => $currentFilter,
                'select' => array('ID'),
            ));
            $storeIds = array();
            foreach ($dbStores as $store) {
                $storeIds[] = $store['ID'];
            }
        } else {
            $storeIds = $request->get('ID');
            if (!is_array($storeIds)) {
                $storeIds = array();
            }
        }

        if (empty($storeIds)) {
            return;
        }

        switch ($action) {
            case 'delete':
                foreach ($storeIds as $storeId) {
                    EmployeeRankTable::delete($storeId);
                }
                break;

            default:
                break;
        }
    }

    private function processServiceActions($currentFilter)
    {
        global $APPLICATION;

        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $params = $request->get('PARAMS');

        if (empty($params['GRID_ID']) || $params['GRID_ID'] != self::GRID_ID) {
            return;
        }

        $action = $request->get('ACTION');

        if (!in_array($action, self::SUPPORTED_SERVICE_ACTIONS)) {
            return;
        }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        switch ($action) {
            case 'GET_ROW_COUNT':
                $count = EmployeeRankTable::getCount($currentFilter);
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => 'AAAA'
                    )
                ));
                break;

            default:
                break;
        }

        die;
    }
    private function validate($upper_code,$code)
{
    $Loi = new ErrorCollection();
    /**
    if ($file_nameF !='')
        {
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
                $check = getimagesize($_FILES["fileToUp"]["tmp_name"]);
                if($check == false) {
                    $Loi->setError(new Error('File is not an image.'));
                    return $Loi;
                }
            // Check if file already exists
            if (file_exists($target_file)) {
                $Loi->setError(new Error('Sorry, file already exists.'));
                return $Loi;
            }
            // Check file size
            if ($_FILES["fileToUp"]["size"] > 1200000) {
                $Loi->setError(new Error('Sorry, your file is too large.'));
                return $Loi;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                $Loi->setError(new Error('Sorry, only JPG, JPEG, PNG & GIF files are allowed.'));
                return $Loi;
            }
        } //if ($filename !='')
        **/
        if($upper_code==""){
             $Loi->setError(new Error('Không được để trống mã cán bộ cấp cha.'));
            return $Loi;
        }
        if($code==""){
             $Loi->setError(new Error('Không được để trống mã cán bộ cấp con.'));
            return $Loi;
        }
return $Loi;
}
private  function ProccessSave($request)
{
    //var_dump($request);die;
    global $DB;
    $save_error = new ErrorCollection();
    $DB->PrepareFields("HR_LIST_POSITION");
    $arFields = array(
            "UPPER_CODE"             =>  "'".trim(strtoupper($request->get('upper_code')))."'",
            "CODE"                    => "'".trim(strtoupper($request->get('code')))."'",
            "STATUS"                  => "'".trim(strtoupper($request->get('status')))."'",
            );
    $DB->Update("HR_LIST_POSITION", $arFields, "WHERE ID='".$request->get('id')."'", $err_mess.__LINE__);
       
}
    private  function ProccessSaveNew($request)
    {
        //var_dump($request);die;
        global $DB;
        $addnew_error = new ErrorCollection();
        /**
        $DB->StartTransaction();
        $id_inserted=$DB->Insert(EmployeeTable::getTableName(),$submited_emp,$ErrInsert_mess.__LINE__);
        $id_inserted=intval($id_inserted);
        if (strlen($ErrInsert_mess)<=0)
        {
            $DB->Commit();  
           
            return $id_inserted; 
        }
        else
        {
            $DB->Rollback();          
            $addnew_error->setError(new Error('Không thể tạo mới nhân viên(Can not insert).'));
            return 0;
        }
    **/
         $arFields = array(
            "UPPER_CODE"             =>  "'".trim(strtoupper($request->get('upper_code')))."'",
            "CODE"                    => "'".trim(strtoupper($request->get('code')))."'",
            "STATUS"                  => "'".trim(strtoupper($request->get('status')))."'",
                );
         $DB->StartTransaction();
         $ID = $DB->Insert("HR_LIST_POSITION", $arFields,$ErrInsert_mess.__LINE__);
         if (strlen($ErrInsert_mess)<=0)
        {
            $DB->Commit();  
           
            return $ID; 
        }
        else
        {
            $DB->Rollback();          
            $addnew_error->setError(new Error('Không thể tạo mới mã cán bộ(Can not insert).'));
            return 0;
        }
    }
}