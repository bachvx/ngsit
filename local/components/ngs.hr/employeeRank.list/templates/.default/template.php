<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;
use Ngs\Hr\Entity\ListTitleTable;
use Ngs\Hr\Entity\StaffLevelTable;
use Ngs\Hr\Entity\TitleTable;
use Ngs\Hr\Helper\Helper;
$this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap-datepicker.standalone.min.css');
// $this->addExternalCss('/local/components/ngs.hr/assets/css/fontawesome-free-5.12.1/css/all.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/chosen.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/chosenImage.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/gijgo.min.css');
// $this->addExternalCss('/local/components/ngs.hr/assets/css/bootstrap-datepicker.min.css');
$this->addExternalCss('/local/components/ngs.hr/assets/css/jquery-ui.css');
$this->addExternalCss('/local/components/ngs.hr/employees.edit/templates/.default/css/style.css');

$APPLICATION->SetTitle('Danh mục mã cán bộ');

$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';

$rows = array();
foreach ($arResult['STORES'] as $store) {
   // var_dump($arParams['URL_TEMPLATES']['EDIT']);die;
    $viewUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['DETAIL'],
        array('ID' => $store['ID'])
    );
    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'],
        array('ID' => $store['ID'])
    );

    $infoLink = '/nhan-su/' . $store['ID'] . '/qua-trinh-cong-tac/';
    $documentLink = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['FILES'],
        array('ID' => $store['ID'])
    );

    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($store['ID']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;
    
    $link_edit='BX.Crm.Page.open(' . Json::encode($viewUrl) . ')';
    //$val_code = array( "id"=>$store['ID'],"upper_code"=>$store['UPPER_CODE'], "code"=>$store['CODE'], "status"=>$store['STATUS']);
    $rows[] = array(
        'id' => $store['ID'],
        'actions' => array(
            array(
                'TITLE' => 'Thêm mới',
                'TEXT' => 'Thêm mới',
                'ONCLICK' => 'view_save();',
                'DEFAULT' => true
            ),
            array(
                'TITLE' => 'Sửa',
                'TEXT' => 'Sửa',
                'ONCLICK' => 'editFunction("'.$store['ID'].'","'.$store['UPPER_CODE'].'","'.$store['CODE'].'","'.$store['STATUS'].'")',
            )
        ),
        'data' => $store,
        'columns' => array(
            'STT' => $store['ID'],
            'UPPER_CODE' => $store['UPPER_CODE'],
            'CODE' => $store['CODE'],
            'STATUS' => $store['STATUS']
        )
    );

}

    $snippet = new Snippet();
?>
   
<?php
$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => array(),
        'EXTENSION' => array(
            'ID' => $gridManagerId,
            'CONFIG' => array(
                'ownerTypeName' => 'STORE',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ),
        ),
        'SHOW_ROW_CHECKBOXES' => false,
       'SHOW_CHECK_ALL_CHECKBOXES' => false, 
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);
?>

<div class="container_level" id="container_level" style="display: none;"></div>
    <script type="text/javascript">
        function vis_form(){
            var x = document.getElementById("container_level");
           if (x.style.display === "block") {
                x.style.display = "none";
            } 
        }
        function editFunction(id,upper_code,code,status) {
            var x = document.getElementById("container_level");
            if (x.style.display === "none") {
                x.style.display = "block";
            } 
           if(status=='có'){
                opt='<option selected value="có">Có</option><option value="Không">Không</option>'
           }else{
                opt='<option selected value="Không">Không</option><option value="Có">Có</option>'
           }
          
           document.getElementById("container_level").innerHTML ='<div class="container_level" id="container_level" style="display: block;"><form class="horizontal-form"  id="frm-add-employee" method="post" enctype="multipart/form-data"><div class="card-header"><div class="row"><div class="col-md-4"><h6>DANH MỤC MÃ CÁN BỘ :: SỬA</h6></div><div class="col-md"-8><div class=" well pagetitle-container pagetitle-align-right-container" style="float: right;"><button type="submit" id="btn_update" class="btn btn-outline-primary float-right form-control" style="width: 70px; margin-right: 2px; height: 32px; padding: 5px;" name="btnsubmit" value="update">Ghi</button><button type="" id="btn_cancel" class="btn btn-outline-primary float-right form-control" style="width: 70px; margin-right: 2px; height: 32px; padding: 5px;" name="btnsubmit" value="" onclick="vis_form();">Hủy</button></div> </div></div></div><div class="material-card card"><div class="card-body wizard-content"><div class="form-group row"><div class="col-md-2"><div class="well" ><label class="lable_input">Mã cấp cán bộ cha</label><label class="cannot-be-empty">(*)</label></div></div><div class="col-md-2"><div class="well" ><input id="upper_code" class="form-control" name="upper_code" type="text" maxlength="15" value="'+ upper_code +'" required/></div></div><div class="col-md-2"><div class="well" ><label class="lable_input">Mã cấp cán bộ con</label><label class="cannot-be-empty">(*)</label></div></div><div class="col-md-2"> <div class="well" > <input id="code" class="form-control" name="code" type="text" maxlength="15"   value="'+ code +'"  required/></div></div><div class="col-md-2"><div class="well" ><label class="lable_input">Tình trạng sử dụng</label>               </div>                           </div>            <div class="col-md-2">                <div class="well" >                                    </div>            <div class="well" >                     <select class="form-control" name="status" id="status">                        ' +  opt + '                    </select>                </div>            </div>                </div>            </div>        </div>  <input type="hidden" id="id" name="id" value="' + id + '">   </form>  </div>';   
        }
       function view_save(){
            var x = document.getElementById("container_level");
            if (x.style.display === "none") {
                x.style.display = "block";
            } 

            document.getElementById("container_level").innerHTML ='<div class="container_level" id="container_level" name="container_level" style="display: block;"><form class="horizontal-form"  id="frm-add-employee" method="post" enctype="multipart/form-data">      <div class="card-header"><div class="row">           <div class="col-md-4"><h6>DANH MỤC MÃ CÁN BỘ :: THÊM MỚI</h6></div>                <div class="col-md"-8><div class=" well pagetitle-container pagetitle-align-right-container" style="float: right;">                 <button type="submit" id="btn_save" class="btn btn-outline-primary float-right form-control" style="width: 70px; margin-right: 2px; height: 32px; padding: 5px;" name="btnsubmit" value="save">Ghi</button>                             <button type="" id="btn_cancel" class="btn btn-outline-primary float-right form-control" style="width: 70px; margin-right: 2px; height: 32px; padding: 5px;" name="btnsubmit" value="" onclick="vis_form();">Hủy</button>                                       </div>                                 </div>                                         </div>                                            </div>        <div class="material-card card">            <div class="card-body wizard-content">                 <div class="form-group row">            <div class="col-md-2">                <div class="well" >                    <label class="lable_input">Mã cấp cán bộ cha</label><label class="cannot-be-empty">(*)</label>                </div>                            </div>            <div class="col-md-2">                                <div class="well" >                    <input id="upper_code" class="form-control" name="upper_code" type="text" maxlength="15"                                        value="" required  title="Mã cán bộ cấp cha không được để trống"/> <span id="upper_code_error" class="help-alert" "display:none"></span>               </div>            </div>                      <div class="col-md-2">                <div class="well" >                    <label class="lable_input">Mã cấp cán bộ con</label><label class="cannot-be-empty">(*)</label>                </div>                            </div>            <div class="col-md-2">                                <div class="well" >                    <input id="code" class="form-control" name="code" type="text" maxlength="15"                                        value=""  required title="Mã cán bộ cấp con không được để trống" /> <span id="code_error" class="help-alert" "display:none"></span>               </div>            </div>        <div class="col-md-2">                <div class="well" >                    <label class="lable_input">Tình trạng sử dụng</label>                </div>                           </div>            <div class="col-md-2">                <div class="well" >                                    </div>            <div class="well" >                     <select class="form-control" name="status" id="status"> <option selected value="có">Có</option><option value="Không">Không</option></select>                </div>            </div>                </div>            </div>        </div>    </form>  </div>';  
       }
       
        $(document).on("submit", "form.container_level", function (e) {
            var elements = document.getElementsByTagName("INPUT");
            alert("Aaa")
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        switch (e.srcElement.id) {
                            case "upper_code":
                                e.target.setCustomValidity("Mã cấp cán bộ cha: Không được để trống");
                                break;
                            case "code":
                                e.target.setCustomValidity("Mã cấp cán bộ con: Không được để trống");
                                break;
                            
                        }
                    }
                };
                elements[i].oninput = function(e) {
                e.target.setCustomValidity("");
        };
            }
        });
        $("#frm-add-employee").on("submit", function(){
           var elements = document.getElementsByTagName("INPUT");

            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        switch (e.srcElement.id) {
                            case "upper_code":
                                e.target.setCustomValidity("Mã cấp cán bộ cha: Không được để trống");
                                break;
                            case "code":
                                e.target.setCustomValidity("Mã cấp cán bộ con: Không được để trống");
                                break;
                            
                        }
                    }
                };
                ;
            }
         })
        $(document).on('change',"#upper_code",function() {
             var str_patt = /^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZa-zA-Z]+$/g
           var patt = new RegExp(/^[a-z0-9]+$/i);
             var cString = $("#upper_code").val();
                var patt = new RegExp(str_patt);
                if (patt.test(cString)) {
                    $('#upper_code').removeClass("has-alert");
                    $("#upper_code_error").text('');
                    $("#upper_code_error").hide();
                } else {
                    $('#upper_code').addClass("has-alert");
                    $("#upper_code_error").text('Nhập lại mã cán bộ cha');
                    $("#upper_code_error").show();
                }
        });
        $(document).on('change',"#code",function() {
             var str_patt = /^[aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZa-zA-Z]+$/g
           var patt = new RegExp(/^[a-z0-9]+$/i);
             var cString = $("#code").val();
                var patt = new RegExp(str_patt);
                if (patt.test(cString)) {
                    $('#code_error').removeClass("has-alert");
                    $("#code_error").text('');
                    $("#code_error").hide();
                } else {
                    $('#code_error').addClass("has-alert");
                    $("#code_error").text('Nhập lại mã cán bộ con');
                    $("#code_error").show();
                }
        });
    </script>

    <style type="text/css">
        .lable_input {
            margin-top: 5px;
        }
        h6{
            font-weight: bold;
            margin: 6px;
        }
    </style>
   <?
    $this->addExternalJs('/local/components/ngs.hr/assets/js/jquery-3.4.1.min.js');
    $this->addExternalJs('/local/components/ngs.hr/assets/js/bootstrap.min.js');
   ?>