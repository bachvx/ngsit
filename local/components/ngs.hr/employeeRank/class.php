<?php
defined('B_PROLOG_INCLUDED') || die;

class CEmployeeRankComponent extends CBitrixComponent
{
    const SEF_DEFAULT_TEMPLATES = array(
        'details' => '#ID#/',
        'edit' => '#ID#/edit/',
//        'qua-trinh-cong-tac' => '#ID#/qua-trinh-cong-tac/',
        'files' => '#ID#/tai-lieu-khac/'
    );

    public function executeComponent()
    {
//        var_dump($this->arParams['PARAMS']);
        if (empty($this->arParams['SEF_MODE']) || $this->arParams['SEF_MODE'] != 'Y') {
            ShowError('SEF_MODE không hoạt động');
            return;
        }

        if (empty($this->arParams['SEF_FOLDER'])) {
            ShowError('SEF_FOLDER trống');
            return;
        }

        if (!is_array($this->arParams['SEF_URL_TEMPLATES'])) {
            $this->arParams['SEF_URL_TEMPLATES'] = array();
        }

        $sefTemplates = array_merge(self::SEF_DEFAULT_TEMPLATES, $this->arParams['SEF_URL_TEMPLATES']);

        $page = CComponentEngine::parseComponentPath(
            $this->arParams['SEF_FOLDER'],
            $sefTemplates,
            $arVariables
        );
//        var_dump($arVariables['ID']);
        if (empty($page)) {
            $page = 'list';
        }

        $this->arResult = array(
            'SEF_FOLDER' => $this->arParams['SEF_FOLDER'],
            'SEF_URL_TEMPLATES' => $sefTemplates,
            'VARIABLES' => $arVariables,
        );

        $this->includeComponentTemplate($page);
    }
}