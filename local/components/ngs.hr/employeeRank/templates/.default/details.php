<?php

use Ngs\Hr\Helper\Helper;

defined('B_PROLOG_INCLUDED') || die;
require $_SERVER['DOCUMENT_ROOT'] . '/local/modules/ngs.hr/include.php';
Helper::setActiveMenu('NHAN_SU');
require $_SERVER['DOCUMENT_ROOT'] . '/local/components/ngs.hr/top_menu/top_menu.php';
Helper::setActiveSubMenu('DANH_SACH_NHAN_VIEN');
require $_SERVER['DOCUMENT_ROOT'] . '/local/components/ngs.hr/top_menu/second_menu.php';
$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'title',
    array(
        'TOOLBAR_ID' => 'CRMSTORES_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => 'Thêm mới',
                'TITLE' => 'Thêm mới',
                'LINK' => CComponentEngine::makePathFromTemplate($urlTemplates['EDIT'], array('ID' => 0)),
                'ICON' => 'btn-add',
            ),
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'ngs.hr:employees.show',
    '',
    array(
        'ID' => $arResult['VARIABLES']['ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);