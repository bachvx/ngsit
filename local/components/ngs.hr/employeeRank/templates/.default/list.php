<?php

use Ngs\Hr\Helper\Helper;

defined('B_PROLOG_INCLUDED') || die;
//require $_SERVER['DOCUMENT_ROOT'] . '/local/components/ngs.hr/top_menu/top_menu.php';

/**
$APPLICATION->IncludeComponent(
    'bitrix:main.interface.buttons',
    '',
    array(
        'ID' => 'STORES',
        'ITEMS' => Helper::getSubMenu(),
    )
); 
*/

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
   'title',
   array(
       'TOOLBAR_ID' => 'CRMSTORES_TOOLBAR',
       'BUTTONS' => array(
           array(
               'TEXT' => 'Thêm mới',
               'TITLE' => 'Thêm mới',
               'ONCLICK' => 'view_save()',
               'ICON' => 'btn-add',
           )
   ),
   $this->getComponent(),
   array('HIDE_ICONS' => 'Y'),
)
);
$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
    'INFO' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['info'],
    'FILES' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['files'],
);

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'title',
    array(),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'ngs.hr:employeeRank.list',
    '',
    array(
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

