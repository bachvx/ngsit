<?php

use Ngs\Hr\Helper\Helper;

require $_SERVER['DOCUMENT_ROOT'] . 'local/components/ngs.hr/top_menu/top_menu.php';
$APPLICATION->IncludeComponent(
    'bitrix:main.interface.buttons',
    '',
    array(
        'ID' => 'STORES',
        'ITEMS' => Helper::getSubMenu('DANH_SACH_NHAN_VIEN')
    )
);
$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);
//$APPLICATION->SetTitle('Thông tin khác');

$APPLICATION->IncludeComponent(
    'ngs.hr:positionLevel',
    '',
    array(
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    )
);