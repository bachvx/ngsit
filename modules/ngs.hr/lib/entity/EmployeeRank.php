<?php

namespace Ngs\Hr\Entity;

use Bitrix\Main\Entity\DataManager;

class EmployeeRankTable extends DataManager
{
    public static function getTableName()
    {
        return 'HR_LIST_POSITION';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'text',
                'primary' => true
            ),
            'STATUS' => array(
                'data_type' => 'text',
            ),
            'UPPER_CODE' => array(
                'data_type' => 'text',
            ),
            'CODE' => array(
                'data_type' => 'text',
            )
        );
    }
}